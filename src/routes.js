// src/routes.js
import React, {  Component } from 'react';
import { Router, BrowserRouter, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history'

//component
import Header from './Component/Header'
import className from './Component/ComponentStandard'
import App from './Component/App'
import About from './Component/About'
import NotFound from './Component/NotFound'

import Login from './Screen/Login'
import Guest from './Screen/Guest'
import LoginTest from './Screen/Test'
import GuestSearch from './Screen/Search/Guest'
import SeeRoom from './Screen/Room/SeeRoom'
import ProviderSearch from './Screen/Search/Provider'
import ProviderCreateRoom from './Screen/Room/Provider/create'
import ProviderEditRoom from './Screen/Room/Provider/edit'
import AdminRoomSearch from './Screen/Search/Admin/room'
import AdminCreateRoom from './Screen/Room/Admin/create'
import AdminEditRoom from './Screen/Room/Admin/edit'
import AdminUserSearch from './Screen/Search/Admin/user'
import AdminCreateUser from './Screen/User/Admin/create'
import AdminEditUser from './Screen/User/Admin/edit'

class Routes extends Component {
  render() {
    return (
      <BrowserRouter>
        <Router history={createBrowserHistory()}>
          <div>
            <Header/>
            <Switch>
                <Route exact path="/" component={App} />
                <Route exact path="/login" component={Login} />
                {/* <Route exact path="/test" component={className} /> */}
                <Route exact path="/test" component={LoginTest} />
                {/* <Route exact path="/guest" component={Guest} /> */}
                <Route exact path="/guest/search" component={GuestSearch} />
                <Route exact path="/guest/room/roomId/:roomId" component={SeeRoom} />
                <Route exact path="/provider/room" component={ProviderSearch} />
                <Route exact path="/provider/room/create" component={ProviderCreateRoom} />
                <Route exact path="/provider/room/roomId/:roomId" component={ProviderEditRoom} />
                <Route exact path="/admin/room" component={AdminRoomSearch} />
                <Route exact path="/admin/room/create" component={AdminCreateRoom} />
                <Route exact path="/admin/room/roomId/:roomId" component={AdminEditRoom} />
                <Route exact path="/admin/user" component={AdminUserSearch} />
                <Route exact path="/admin/user/create" component={AdminCreateUser} />
                <Route exact path="/admin/user/userId/:userId" component={AdminEditUser} />
                <Route path="*" component={NotFound} />
            </Switch>
          </div>
        </Router >
      </BrowserRouter>
    )
  }
}

export default Routes