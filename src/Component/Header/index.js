// src/Component/Header/index.js
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import Spinner from '../Loader'

import { Menu, Segment } from 'semantic-ui-react'

import './style.css';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,

            response: [],
            responsePackageReceive: 0,
            responsePackageAll: 0,
            
            data: null,
            /////////////
            activeItem: 'home',
            roleId: 0,

            refresh: '',
            logoImage: 'http://www.soc.ku.ac.th/logo/kub.png'
        }
    }

    componentDidMount() {
        this.setUp()
    }

    componentDidUpdate() {
        if(localStorage.getItem('header-update')) {
            localStorage.removeItem('header-update')
            this.setUp()
        }
    }

    setUp = () => {
        let apiList = [
            this.getMyRole
        ]
        // this.callApiBySequence(apiList)
        this.callApiByParallel(apiList)
    }

    getMyRole = () => {
        return this.fetchData(
                    'http://localhost:5000/role', 
                    res => {
                        console.log(res)
                        this.setState({
                            roleId: res.roleId ? res.roleId : 0
                        })
                    }
                )
    }

    getMyRoleWhenLogout = () => {
        return this.fetchData(
                    'http://localhost:5000/role', 
                    res => {
                        console.log(res)
                        this.setState({
                            roleId: res.roleId ? res.roleId : 0
                        })
                        this.redirect('/guest/search')
                    }
                )
    }

    logout = () => {
        localStorage.removeItem('ku-room-token')
        let apiList = [
            this.getMyRoleWhenLogout
        ]
        this.callApiByParallel(apiList)  
    }

/////////////this is function set/////////////

    callApiByParallel = (fetchFuncList) => {
        this.setState({
            responsePackageAll: fetchFuncList.length,
        })
        this.setState({
            isLoading: false
        })
        fetchFuncList.forEach( fetchFunc => {
            fetchFunc()
        })
    }

    callApiBySequence = (fetchFuncList, index=0) => {
        if(!fetchFuncList[index]) {
            return this.setState({isLoading: false})
        }

        fetchFuncList[index]()
                .then( () => this.callApiBySequence(fetchFuncList, index+1))

    }

    fetchData = (url='/default', interactData=()=>{}, method='GET', data={}) => {
        return fetch(url, this.httpOption(method, data))
                .then(res => res.json())
                .then(res => {
                    interactData(res)
                    return res
                })
                .then( res => {
                    // let array = this.state.response
                    // array.push(res)
                    // this.setState({response: array})
                    this.setState({responsePackageReceive: this.state.responsePackageReceive+1})
                    return res
                })
    }

    httpOption = (method, data) => {
        let option = {
            method, // *GET, POST, PUT, DELETE, etc.
            // mode: "cors", // no-cors, cors, *same-origin
            // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            // redirect: "follow", // manual, *follow, error
            // referrer: "no-referrer", // no-referrer, *client
        }

        const token = localStorage.getItem('ku-room-token')
        if(token) {
            option.headers['ku-room-token'] = token
        }

        let isMethodWithBody = false
        let methodWithBodyList = ['POST', 'PUT']
        methodWithBodyList.forEach( methodWithBody => {
            let logic = method === methodWithBody
            isMethodWithBody = isMethodWithBody || logic
        })
        if(isMethodWithBody) {
            option.body = JSON.stringify(data) // body data type must match "Content-Type" header
        }
        
        return option
    }

    redirect = (path='/', isReplacePageHistory=false) => {
        if(!isReplacePageHistory) this.props.history.push(path)
        else this.props.history.replace(path)
    }

/////////////this is function set/////////////

    renderHeader = () => {
        switch(this.state.roleId) {
            case 0:
                return this.renderGuestHeader()
                break;
            case 1:
                return this.renderProviderHeader()
                break;
            case 2:
                return this.renderAdminHeader()
                break;
            default:
                console.log('header error')
                return this.renderGuestHeader()
        }
    }

    renderGuestHeader = () => {
        let {logoImage ,activeItem} = this.state
        return (
            <Menu inverted secondary color={'green'}>
                <Menu.Item>
                    <img src={logoImage} style={{width:'3vw'}} />
                </Menu.Item>
                <Menu.Item
                    name='search'
                    active={activeItem === 'search'}
                    onClick={()=>this.redirect('/guest/search')}
                />
                <Menu.Menu position='right'>
                <Menu.Item
                    name='Login (staff only)'
                    active={activeItem === 'login'}
                    onClick={()=>this.redirect('/login')}
                />
                </Menu.Menu>
            </Menu>
        )
    }

    renderProviderHeader = () => {
        let {logoImage, activeItem} = this.state
        return (
            <Menu inverted secondary color={'green'}>
                <Menu.Item>
                    <img src={logoImage} style={{width:'3vw'}}/>
                </Menu.Item>
                <Menu.Item
                    name='my room'
                    active={activeItem === 'my room'}
                    onClick={()=>this.redirect('/provider/room')}
                />
                <Menu.Item
                    name='create room'
                    active={activeItem === 'create room'}
                    onClick={()=>this.redirect('/provider/room/create')}
                />
                <Menu.Menu position='right'>
                <Menu.Item
                    name='Logout (staff only)'
                    active={activeItem === 'logout'}
                    onClick={()=>this.logout()}
                />
                </Menu.Menu>
            </Menu>
        )
    }

    renderAdminHeader = () => {
        let {logoImage, activeItem} = this.state
        return (
            <Menu inverted secondary color={'green'}>
                <Menu.Item>
                    <img src={logoImage} style={{width:'3vw'}}/>
                </Menu.Item>
                <Menu.Item
                    name='user'
                    active={activeItem === 'user'}
                    onClick={()=>this.redirect('/admin/user')}
                />
                <Menu.Item
                    name='create user'
                    active={activeItem === 'create user'}
                    onClick={()=>this.redirect('/admin/user/create')}
                />
                <Menu.Item
                    name='room'
                    active={activeItem === 'room'}
                    onClick={()=>this.redirect('/admin/room')}
                />
                <Menu.Item
                    name='create room'
                    active={activeItem === 'create room'}
                    onClick={()=>this.redirect('/admin/room/create')}
                />

                <Menu.Menu position='right'>
                <Menu.Item
                    name='Logout (staff only)'
                    active={activeItem === 'logout'}
                    onClick={()=>this.logout()}
                />
                </Menu.Menu>
            </Menu>
        )
    }

    handleItemClick = (e, { name }) => {
        this.setState({ activeItem: name })
        
    }



    render() {
        let {isLoading} = this.state
        let {responsePackageReceive, responsePackageAll} = this.state

        if(isLoading || responsePackageReceive < responsePackageAll) return <Spinner/>
        /////////////
        let {roleId} = this.state

        return (
            <div style={{marginBottom: '2vh'}}>
                <Segment inverted color={'green'} style={{padding: '5px'}}>
                    {this.renderHeader()}
                </Segment>                
            </div>
        )
    
    }
}

export default withRouter(Header)