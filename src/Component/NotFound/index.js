// src/components/NotFound/index.js
import React, {  Component } from 'react'
import { Redirect } from 'react-router-dom'
import './style.css'

class NotFound extends Component {

  componentDidMount() {
      
  }

  render() {
    return <Redirect to='/' />
    // return (
    //   <div>
    //     <h1>404 : Not Found :(</h1>
    //   </div>
    // )
  }
}

export default NotFound;