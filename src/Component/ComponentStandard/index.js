// src/Component/ClassName/index.js
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import Spinner from '../Loader'

import { Button } from 'semantic-ui-react'

import './style.css';

class ClassName extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,

            response: [],
            responsePackageReceive: 0,
            responsePackageAll: 0,
            
            data: []
            /////////////

        }
    }

    componentDidMount() {
        this.setUp()
    }

    setUp = () => {
        let apiList = [
            this.getAllRoom,
            this.postRoute
        ]
        // this.callApiBySequence(apiList)
        this.callApiByParallel(apiList)
    }

    getAllRoom = () => {
        return this.fetchData(
                    'http://localhost:5000/room/all', 
                    res => {this.setState({data: res})}
                )
    }

    postRoute = () => {
        return this.fetchData(
                    'http://localhost:5000/default', 
                    res => {},
                    'POST',
                    {data: 'my data send'}
                )
    }

/////////////this is function set/////////////

    callApiByParallel = (fetchFuncList) => {
        this.setState({
            responsePackageAll: fetchFuncList.length,
        })
        this.setState({
            isLoading: false
        })
        fetchFuncList.forEach( fetchFunc => {
            fetchFunc()
        })
    }

    callApiBySequence = (fetchFuncList, index=0) => {
        if(!fetchFuncList[index]) {
            return this.setState({isLoading: false})
        }

        fetchFuncList[index]()
                .then( () => this.callApiBySequence(fetchFuncList, index+1))

    }

    fetchData = (url='/default', interactData=()=>{}, method='GET', data={}) => {
        return fetch(url, this.httpOption(method, data))
                .then(res => res.json())
                .then(res => {
                    interactData(res)
                    return res
                })
                .then( res => {
                    // let array = this.state.response
                    // array.push(res)
                    // this.setState({response: array})
                    this.setState({responsePackageReceive: this.state.responsePackageReceive+1})
                    return res
                })
    }

    httpOption = (method, data) => {
        let option = {
            method, // *GET, POST, PUT, DELETE, etc.
            // mode: "cors", // no-cors, cors, *same-origin
            // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            // redirect: "follow", // manual, *follow, error
            // referrer: "no-referrer", // no-referrer, *client
        }

        let isMethodWithBody = false
        let methodWithBodyList = ['POST', 'PUT']
        methodWithBodyList.forEach( methodWithBody => {
            let logic = method === methodWithBody
            isMethodWithBody = isMethodWithBody || logic
        })
        if(isMethodWithBody) {
            option.body = JSON.stringify(data) // body data type must match "Content-Type" header
        }
        
        return option
    }

    redirect = (path='/', isReplacePageHistory=false) => {
        if(!isReplacePageHistory) this.props.history.push(path)
        else this.props.history.replace(path)
    }

/////////////this is function set/////////////

    buttonSet = (onClick=()=>{}, content='default') => {
        return (
            <div>
                <Button positive onClick={()=>onClick()}>
                    {content}
                </Button>
            </div>
        )
    }

    standardRender = () => {
        let {roomList} = this.state.data
        return (
            <div>
                {roomList[0].roomName}
                {this.buttonSet(
                    () => this.redirect(),
                    'redirect'
                )}
            </div>
        )
    }

    render() {
        let {isLoading} = this.state
        let {responsePackageReceive, responsePackageAll} = this.state

        if(isLoading || responsePackageReceive < responsePackageAll) return <Spinner/>
        /////////////
        return (
            <div>
                {this.standardRender()}
            </div>
        )
    }
}

export default withRouter(ClassName)