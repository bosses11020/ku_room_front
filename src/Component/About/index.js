// src/components/App/index.js
import React, { Component } from 'react';
import './style.css';

class About extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,

            data: null
        }
    }

    componentDidMount() {
        fetch('http://localhost:8080/room/all')
        .then(res => res.json())
        .then(res => {
            console.log(res)
            this.setState({data: res, isLoading: false})
        })
    }

    tableRoomList = (roomList) => {
        let html = []
        roomList.forEach( room => {
            let htmlRow = []
            let htmlItem = []
            Object.values(room).forEach( value => {
                htmlItem.push(<td>{value}</td>)
            })
            htmlRow.push(<tr>{htmlRow}</tr>)
            html.push(htmlRow)
        })
        console.log(html)

        return null
    }

    render() {
        if(this.state.isLoading) return null
        return (
            <div>
                <h1>About Page</h1>
                <div>
                    {this.state.data[0].roomName}
                </div>
                {/* <div>
                    {this.tableRoomList(this.state.data)}
                </div> */}
            </div>
        )
    }
}

export default About;