// src/Component/Login/index.js
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import Spinner from '../../Component/Loader'

import { Button, Form, Segment } from 'semantic-ui-react'

import './style.css';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,

            response: [],
            responsePackageReceive: 0,
            responsePackageAll: 0,
            
            data: {
                username: '',
                password: ''
            }
            /////////////
                    
            
        }
    }

    login = () => {
        let apiList = [
            this.loginApi
        ]
        this.callApiByParallel(apiList)
    }

    loginApi = () => {
        let {data} = this.state
        console.log(data)
        return this.fetchData(
                    'http://localhost:5000/login', 
                    res => {
                        console.log(res)
                        if(res.token) {
                            localStorage.setItem('ku-room-token', res.token)
                            localStorage.setItem('header-update', true)
                            if(res.roleId === 1) {
                                this.redirect('/provider/room', true)
                            }
                            else if(res.roleId === 2) {
                                this.redirect('/admin/room', true)
                            }
                        }
                    },
                    'POST',
                    {...data}
                )
    }


/////////////this is function set/////////////

    callApiByParallel = (fetchFuncList) => {
        this.setState({
            responsePackageAll: fetchFuncList.length,
            isLoading: true
        })
        fetchFuncList.forEach( fetchFunc => {
            fetchFunc()
        })
        this.setState({
            isLoading: false
        })
    }

    callApiBySequence = (fetchFuncList, index=0) => {
        if(index === 0) {
            this.setState({
                responsePackageAll: fetchFuncList.length,
                isLoading: true
            })
        }
        if(!fetchFuncList[index]) {
            return this.setState({isLoading: false})
        }

        fetchFuncList[index]()
                .then( () => this.callApiBySequence(fetchFuncList, index+1))

    }

    fetchData = (url='/default', interactData=()=>{}, method='GET', data={}) => {
        return fetch(url, this.httpOption(method, data))
                .then(res => res.json())
                .then(res => {
                    interactData(res)
                    return res
                })
                .then( res => {
                    // let array = this.state.response
                    // array.push(res)
                    // this.setState({response: array})
                    this.setState({responsePackageReceive: this.state.responsePackageReceive+1})
                    return res
                })
    }

    httpOption = (method, data) => {
        let option = {
            method, // *GET, POST, PUT, DELETE, etc.
            // mode: "no-cors", // no-cors, cors, *same-origin
            // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
                // "ku-room-token": localStorage.getItem('ku-room-token')
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            // redirect: "follow", // manual, *follow, error
            // referrer: "no-referrer", // no-referrer, *client
        }

        const token = localStorage.getItem('ku-room-token')
        if(token) {
            option.headers['ku-room-token'] = token
        }

        let isMethodWithBody = false
        const methodWithBodyList = ['POST', 'PUT']
        methodWithBodyList.forEach( methodWithBody => {
            let logic = method === methodWithBody
            isMethodWithBody = isMethodWithBody || logic
        })
        if(isMethodWithBody) {
            option.body = JSON.stringify(data) // body data type must match "Content-Type" header
        }
        
        return option
    }

    redirect = (path='/', isReplacePageHistory=false) => {
        if(!isReplacePageHistory) this.props.history.push(path)
        else this.props.history.replace(path)
    }

/////////////this is function set/////////////

    buttonSet = (onClick=()=>{}, content='default') => {
        return (
            <div>
                <Button positive onClick={()=>onClick()}>
                    {content}
                </Button>
            </div>
        )
    }

    standardRender = () => {
        let {data} = this.state
        return (
            <div>
                {data[0].roomName}
                {this.buttonSet(
                    () => this.redirect(),
                    'redirect'
                )}
            </div>
        )
    }

    handleChangeUsername = (event) => {
        let {data} = this.state
        this.setState({
            data: {
                ...data,
                username: event.target.value
            }
        })
    }
    
    handleChangePassword = (event) => {
        let {data} = this.state
        this.setState({
            data: {
                ...data,
                password: event.target.value
            }
        })
    }

    render() {
        let {isLoading} = this.state
        let {responsePackageReceive, responsePackageAll} = this.state

        if(isLoading || responsePackageReceive < responsePackageAll) return <Spinner/>
        /////////////
        let {username, password} = this.state.data

        return (
            <div>
                <Segment placeholder>
                    <Form>
                        <Form.Input icon='user' iconPosition='left' label='Username' placeholder='Username' 
                        value={username} onChange={this.handleChangeUsername}/>
                        <Form.Input icon='lock' iconPosition='left' label='Password' type='password' 
                        value={password} onChange={this.handleChangePassword}/>

                        <Button content='Login' primary  onClick={() => this.login()}/>
                    </Form>
                </Segment>
            </div>
        )
    }
}

export default withRouter(Login)