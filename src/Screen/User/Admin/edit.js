// src/Component/Login/index.js
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import Spinner from '../../../Component/Loader'

import { Form, Input, Select, Segment, Divider
        , Header, Button, Modal, Icon } from 'semantic-ui-react'
import {campusList} from '../../../Common/department'

import './style.css'

class AdminEditUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,

            response: [],
            responsePackageReceive: 0,
            responsePackageAll: 0,
            
            data: {
                roleId: 1,
                username: '',
                password: '',

                campus: null,
                faculty: null,

                telNo: '',
                email: '',
                facebook: '',
                lineId: '',
            },
            /////////////
            userId: this.props.match.params.userId,

            optionsCampus: [],
            optionsFacultyOfCampus: {},

            isRequireModalOpen: false,

            requireField: {
                isEmptyRoleId: false,
                isEmptyUsername: false,
                isEmptyPassword: false,
                
                isEmptyTelNo: false,
                isEmptyEmail: false,
            }
          
        }
    }

    componentDidMount() {
        this.setup()
    }

    setup = () => {
        this.setState({
            startState: this.state
        })

        let optionsCampus = [{value: null, text: 'ไม่เลือก'}]
        let optionsFacultyOfCampus = { 'null': [{value: null, text: 'ไม่เลือก'}] }

        for(let i=0 ; i<campusList.length ; i++) {
            let campus = campusList[i].campus

            optionsCampus
            .push({
                value: campus,
                text: campus
            })

            let facultyList = campusList[i].facultyList
            let optionsFaculty = [{value: null, text: 'ไม่เลือก'}]
            for(let j=0 ; j<facultyList.length ; j++) {
                optionsFaculty
                .push({
                    value: facultyList[j].faculty,
                    text: facultyList[j].faculty
                })
            }

            optionsFacultyOfCampus[`${campus}`] = optionsFaculty
        }

        this.setState({
            optionsCampus,
            optionsFacultyOfCampus
        })

        this.searchUser()

    }

    componentDidUpdate() {
        console.log(this.state)
    }

    updateUser = () => {
        let data = this.state.data
        let userId = this.state.userId
        let body = {
            ...data, userId
        }

        let apiList = [
            () => this.updateUserApi(body)
        ]

        this.callApiByParallel(apiList)
    }

    updateUserApi = (body) => {
        return this.fetchData(
            'http://localhost:5000/user/update', 
            res => {
                console.log(res)
                this.redirect('/admin/user')
            },
            'POST',
            {...body}
        )
    }

    searchUser = () => {
        let body = {
            userId: this.state.userId
        }

        let apiList = [
            () => this.searchUserApi(body)
        ]

        this.callApiByParallel(apiList)
    }

    setupUserData = (user) => {
        let data = this.state.data

        for (var key in user) {
            if(data.hasOwnProperty(key)) {
                data[key] = user[key]
            }
        }

        this.setState({
            data: { ...this.state.data,
                ...data,
            }
        })
    }

    searchUserApi = (body) => {
        return this.fetchData(
            'http://localhost:5000/user/search', 
            res => {
                console.log(res)
                this.setupUserData(res.userList[0])
                // this.redirect('/admin/user')
            },
            'POST',
            {...body}
        )
    }

/////////////this is function set/////////////

    callApiByParallel = (fetchFuncList) => {
        this.setState({
            responsePackageAll: fetchFuncList.length,
            isLoading: true
        })
        fetchFuncList.forEach( fetchFunc => {
            fetchFunc()
        })
        this.setState({
            isLoading: false
        })
    }

    callApiBySequence = (fetchFuncList, index=0) => {
        if(index === 0) {
            this.setState({
                responsePackageAll: fetchFuncList.length,
                isLoading: true
            })
        }
        if(!fetchFuncList[index]) {
            return this.setState({isLoading: false})
        }

        fetchFuncList[index]()
                .then( () => this.callApiBySequence(fetchFuncList, index+1))

    }

    fetchData = (url='/default', interactData=()=>{}, method='GET', data={}) => {
        return fetch(url, this.httpOption(method, data))
                .then(res => res.json())
                .then(res => {
                    interactData(res)
                    return res
                })
                .then( res => {
                    // let array = this.state.response
                    // array.push(res)
                    // this.setState({response: array})
                    this.setState({responsePackageReceive: this.state.responsePackageReceive+1})
                    return res
                })
    }

    httpOption = (method, data) => {
        let option = {
            method, // *GET, POST, PUT, DELETE, etc.
            // mode: "no-cors", // no-cors, cors, *same-origin
            // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
                // "ku-room-token": localStorage.getItem('ku-room-token')
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            // redirect: "follow", // manual, *follow, error
            // referrer: "no-referrer", // no-referrer, *client
        }

        const token = localStorage.getItem('ku-room-token')
        if(token) {
            option.headers['ku-room-token'] = token
        }

        let isMethodWithBody = false
        const methodWithBodyList = ['POST', 'PUT']
        methodWithBodyList.forEach( methodWithBody => {
            let logic = method === methodWithBody
            isMethodWithBody = isMethodWithBody || logic
        })
        if(isMethodWithBody) {
            option.body = JSON.stringify(data) // body data type must match "Content-Type" header
        }
        
        return option
    }

    redirect = (path='/', isReplacePageHistory=false) => {
        if(!isReplacePageHistory) this.props.history.push(path)
        else this.props.history.replace(path)
    }

/////////////this is function set/////////////

    handleChangeRoleId = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            roleId: value
        }
    })
    handleChangeUsername = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            username: value
        }
    })
    handleChangePassword = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            password: value
        }
    })
    handleChangeCampus = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            campus: value,
            faculty: null
        }
    })
    handleChangeFaculty = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            faculty: value
        }
    })
    handleChangeTelNo = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            telNo: value
        }
    })
    handleChangeEmail = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            email: value
        }
    })
    handleChangeFacebook = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            facebook: value
        }
    })
    handleChangeLineId = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            lineId: value
        }
    })


    dataHaveEmptyString = (data, exceptionItemList=[]) => {
        let isDataHaveEmptyString = false

        for (var key in data) {
            if(exceptionItemList.includes(key)) {
                continue
            }
            else if(data[key]==='') {
                isDataHaveEmptyString = true
                break
            }
        }
        return isDataHaveEmptyString
    }

    isVoid = (data) => {
        if(data==='' || data===null || data===undefined) {
            return true
        }
        else {
            return false
        }
    }

    updateErrorRequireField = () => {
        let {roleId, username, password, telNo, email} = this.state.data

        this.setState({ requireField: { ...this.state.requireField,
            isEmptyRoleId: this.isVoid(roleId),
            isEmptyUsername: this.isVoid(username),
            isEmptyPassword: this.isVoid(password),
            isEmptyTelNo: this.isVoid(telNo),
            isEmptyEmail: this.isVoid(email),
        }})
    }

    handleCreateRoom = () => {
        let data = this.state.data
        let exceptionItemList = ['campus', 'faculty', 'facebook', 'lineId']

        let condition = this.dataHaveEmptyString(data, exceptionItemList)
        this.updateErrorRequireField()
        
        if(condition) {
            this.openRequireModal()
        }
        else {
            console.log('data is ready for send')
            console.log(data)
            this.updateUser()
        }
    }

    openRequireModal = () => {
        this.setState({ isRequireModalOpen: true })
    }

    closeRequireModal = () => {
        this.setState({ isRequireModalOpen: false })
    }

    renderRequireModal = () => {
        return (
            <Modal 
            centered={false}
            style={{ position: 'static', marginTop: '30vh' }}
            open={this.state.isRequireModalOpen}
            onClose={this.closeRequireModal} 
            basic 
            size='small'
            >
                <Header icon='remove' content='ข้อมูลไม่ครบถ้วน' style={{color:'indianred'}}/>
                <Modal.Content>
                <p>
                    กรุณากรอกข้อมูลให้ครบถ้วน ตามช่องที่มีเครื่องหมาย ( * ) 
                </p>
                </Modal.Content>
                <Modal.Actions>
                <Button basic color='green' inverted onClick={this.closeRequireModal}>
                    <Icon name='backward' /> Back
                </Button>
                </Modal.Actions>
            </Modal>
        )
        
    }

    render() {
        let {isLoading} = this.state
        let {responsePackageReceive, responsePackageAll} = this.state

        if(isLoading || responsePackageReceive < responsePackageAll) return <Spinner/>
        /////////////

        let {username, password, roleId, campus, faculty, telNo, email, facebook, lineId} = this.state.data
        const {optionsCampus, optionsFacultyOfCampus} = this.state
        let optionsFaculty = optionsFacultyOfCampus[`${campus}`]
        let {isEmptyRoleId, isEmptyUsername, isEmptyPassword, isEmptyTelNo, isEmptyEmail} = this.state.requireField

        const roleIdOptions = [
            {key: 'provider', text: 'หน่วยงาน', value: 1},
            {key: 'admin', text: 'ผู้ดูแลระบบ', value: 2}
        ]
        

        return (
            <div style={{margin:'0vw 10vw 0 10vw'}}>

                {this.renderRequireModal()}
                
                <Segment>
                <Form>

                    <Divider horizontal>
                    <Header as='h5'>
                        ข้อมูลผู้ใช้
                    </Header>
                    </Divider>

                    <Form.Group>
                    <Form.Field
                        width={6}
                        control={Input}
                        required

                        label='ชื่อผู้ใช้'
                        placeholder='ชื่อผู้ใช้'

                        value={username}
                        error={isEmptyUsername}
                        onChange={this.handleChangeUsername}
                    />
                    <Form.Field
                        width={6}
                        control={Input}
                        required

                        label='รหัสผ่าน'
                        placeholder='รหัสผ่าน'

                        value={password}
                        error={isEmptyPassword}
                        onChange={this.handleChangePassword}
                    />
                    </Form.Group>

                    <Form.Group widths='equal'>
                    <Form.Field
                        control={Select}
                        required

                        label={{ children: 'สถานะผู้ใช้'}}
                        placeholder='สถานะผู้ใช้'
                        
                        options={roleIdOptions}
                        defaultValue={1}
                        value={roleId}
                        error={isEmptyRoleId}
                        onChange={this.handleChangeRoleId}
                    />
                    <Form.Field
                        control={Select}
                        searchInput={{ id: 'form-select-control-campus' }}
                        label={{ children: 'วิทยาเขต', htmlFor: 'form-select-control-campus' }}
                        placeholder='วิทยาเขต'
                        search

                        options={optionsCampus} 
                        value={campus}
                        onChange={this.handleChangeCampus}
                    />
                    <Form.Field
                        control={Select}
                        searchInput={{ id: 'form-select-control-faculty' }}
                        label={{ children: 'คณะ หน่วยงาน', htmlFor: 'form-select-control-faculty' }}
                        placeholder='คณะ หน่วยงาน'
                        search

                        options={optionsFaculty} 
                        value={faculty}
                        onChange={this.handleChangeFaculty}
                    />
                    </Form.Group>

                    <Divider horizontal style={{margin:'1.5rem 0'}}>
                    <Header as='h5'>
                        ข้อมูลสำหรับติดต่อ
                    </Header>
                    </Divider>

                    <Form.Group widths='equal'>
                    <Form.Field
                        control={Input}
                        required

                        label='เบอร์โทรศัพท์'
                        placeholder='เบอร์โทรศัพท์'

                        value={telNo}
                        error={isEmptyTelNo}
                        onChange={this.handleChangeTelNo}
                    />
                    <Form.Field
                        control={Input}
                        required

                        label='อีเมล'
                        placeholder='อีเมล'

                        value={email}
                        error={isEmptyEmail}
                        onChange={this.handleChangeEmail}
                    />
                    <Form.Field
                        control={Input}

                        label='บัญชี เฟสบุ๊ค'
                        placeholder='ชืัญชี เฟสบุ๊ค'

                        value={facebook}
                        onChange={this.handleChangeFacebook}
                    />
                    <Form.Field
                        control={Input}

                        label='ไลน์ ไอดี'
                        placeholder='ไลน์ ไอดี'

                        value={lineId}
                        onChange={this.handleChangeLineId}
                    />
                    </Form.Group>

                </Form>        
                </Segment>     

                <div style={{display:'flex', justifyContent:'space-between'}}>
                    <div>
                    </div>
                    
                    <Button
                        style={{maxHeight:'42px'}}
                        size='large'
                        color='green'
                        attached='bottom'
                        content='Save '
                        onClick={this.handleCreateRoom}
                    />
                </div>   

            </div>
        )
    }
}

export default withRouter(AdminEditUser)