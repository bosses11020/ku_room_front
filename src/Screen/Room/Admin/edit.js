// src/Component/Login/index.js
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import Spinner from '../../../Component/Loader'

import { Form, Input, TextArea, Select, Segment, Divider
        , Header, Checkbox, Button, Image, Modal, Icon } from 'semantic-ui-react'
import {provinceList} from './province'

import './style.css'

class AdminEditRoom extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,

            response: [],
            responsePackageReceive: 0,
            responsePackageAll: 0,
            
            data: {
                buildingName: '',
                roomName: '',
                costPerHour: '',
                costPerDay: '',
                capacity: '',
                province: 'กรุงเทพมหานคร',
                googleMapUrl: '',
                location: '',

                roomMainPicture: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRO16IDz68_ChB0bL0KVMaqHOtB_ts835Io5cAWd40ZKS2QRL_w',

                boolean: {
                    airCondition: false, 
                    fan: false, 
                    projector: false, 
                    whiteBoard: false, 
                    blackBoard: false, 
                    audioSystem: false, 
                    computer: false, 
                    wifi: false, 
                    refrigerator: false, 
                    kitchen: false, 
                    microwave: false, 
                    televistion: false, 
                    fitness: false, 
                    pool: false
                }
            },
            /////////////
            userId: 0,
            roomId: this.props.match.params.roomId,
            roomTypeId: 1,

            optionsProvince: [],
            optionsUser: [],

            isRequireModalOpen: false,

            requireField: {
                isEmptyRoomTypeId: false,

                isEmptyBuildingName: false,
                isEmptyRoomName: false,

                isEmptyCostPerHour: false,
                isEmptyCostPerDay: false,
                isEmptyCapacity: false,

                isEmptyProvince: false,
            }
          
        }
    }

    componentDidMount() {
        this.setup()
    }

    setup = () => {
        this.setState({
            startState: this.state
        })
        let optionsProvince = []
        provinceList.forEach( province => {
            optionsProvince
            .push({
                value: province.name, 
                text: province.name
            })
        })

        this.setState({
            optionsProvince
        })

        let apiList = [
            () => this.getRoomByIdApi(this.state.roomId),
            () => this.getAllUserApi()
        ]

        this.callApiByParallel(apiList)
    }

    componentDidUpdate() {
        console.log(this.state)
    }

    updateRoom = (data) => {
       let body = data
        let apiList = [
            () => this.updateRoomApi(body)
        ]

        this.callApiByParallel(apiList)
    }

    updateRoomApi = (body) =>{
        return this.fetchData(
            'http://localhost:5000/room/update', 
            res => {
                console.log(res)
                this.redirect('/admin/room')
            },
            'POST',
            {...body}
        )
    }

    setupRoomData = (room) => {
        let data = this.state.data
        let boolean = this.state.data.boolean

        for (var key in room) {
            if(data.hasOwnProperty(key)) {
                data[key] = room[key]
            }
            else if(boolean.hasOwnProperty(key)) {
                boolean[key] = room[key]
            }
        }

        this.setState({
            userId: room.userId,
            roomTypeId: room.roomTypeId,
            data: { ...this.state.data,
                ...data,
                boolean: { ...this.state.boolean,
                    ...boolean
                }
            }
        })
    }

    getRoomByIdApi = (roomId) => {
        return this.fetchData(
            `http://localhost:5000/room/roomId/${roomId}`, 
            res => {
                // console.log(res)
                this.setupRoomData(res.room)
            },
        )
    }

    setupUserData = (userList) => {
        let optionsUser = []
        userList.forEach( user => {
            optionsUser
            .push({
                value: user.id, 
                text: user.username
            })
        })
        this.setState({optionsUser})
    }

    getAllUserApi = () => {
        return this.fetchData(
            `http://localhost:5000/user/all`, 
            res => {
                console.log(res)
                this.setupUserData(res.userList)
            },
        )
    }

/////////////this is function set/////////////

    callApiByParallel = (fetchFuncList) => {
        this.setState({
            responsePackageAll: fetchFuncList.length,
            isLoading: true
        })
        fetchFuncList.forEach( fetchFunc => {
            fetchFunc()
        })
        this.setState({
            isLoading: false
        })
    }

    callApiBySequence = (fetchFuncList, index=0) => {
        if(index === 0) {
            this.setState({
                responsePackageAll: fetchFuncList.length,
                isLoading: true
            })
        }
        if(!fetchFuncList[index]) {
            return this.setState({isLoading: false})
        }

        fetchFuncList[index]()
                .then( () => this.callApiBySequence(fetchFuncList, index+1))

    }

    fetchData = (url='/default', interactData=()=>{}, method='GET', data={}) => {
        return fetch(url, this.httpOption(method, data))
                .then(res => res.json())
                .then(res => {
                    interactData(res)
                    return res
                })
                .then( res => {
                    // let array = this.state.response
                    // array.push(res)
                    // this.setState({response: array})
                    this.setState({responsePackageReceive: this.state.responsePackageReceive+1})
                    return res
                })
    }

    httpOption = (method, data) => {
        let option = {
            method, // *GET, POST, PUT, DELETE, etc.
            // mode: "no-cors", // no-cors, cors, *same-origin
            // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
                // "ku-room-token": localStorage.getItem('ku-room-token')
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            // redirect: "follow", // manual, *follow, error
            // referrer: "no-referrer", // no-referrer, *client
        }

        const token = localStorage.getItem('ku-room-token')
        if(token) {
            option.headers['ku-room-token'] = token
        }

        let isMethodWithBody = false
        const methodWithBodyList = ['POST', 'PUT']
        methodWithBodyList.forEach( methodWithBody => {
            let logic = method === methodWithBody
            isMethodWithBody = isMethodWithBody || logic
        })
        if(isMethodWithBody) {
            option.body = JSON.stringify(data) // body data type must match "Content-Type" header
        }
        
        return option
    }

    redirect = (path='/', isReplacePageHistory=false) => {
        if(!isReplacePageHistory) this.props.history.push(path)
        else this.props.history.replace(path)
    }

/////////////this is function set/////////////

    handleChangeRoomTypeId = (e, { value }) => this.setState({ 
        roomTypeId: value
    })
    handleChangeBuildingName = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            buildingName: value
        } 
    })
    handleChangeRoomName = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            roomName: value
        } 
    })
    handleChangeCostPerHour = (e, { value }) => {
        if( ( e.target.validity.valid && (e.target.value!=='0'||this.state.data.costPerHour!=='') ) 
        || e.target.value === '') {
            this.setState({ 
                data: {
                    ...this.state.data, 
                    costPerHour: e.target.value
                } 
            })
        }
    }
    handleChangeCostPerDay = (e, { value }) => {
        if( ( e.target.validity.valid && (e.target.value!=='0'||this.state.data.costPerHour!=='') ) 
        || e.target.value === '') {
            this.setState({ 
                data: {
                    ...this.state.data, 
                    costPerDay: e.target.value
                } 
            })
        }
    } 
    handleChangeCapacity = (e, { value }) => {
        if( ( e.target.validity.valid && (e.target.value!=='0'||this.state.data.costPerHour!=='') ) 
        || e.target.value === '') {
            this.setState({ 
                data: {
                    ...this.state.data, 
                    capacity: e.target.value
                } 
            })
        }
    }
    
    handleChangeProvince = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            province: value
        } 
    })
    handleChangeGoogleMapUrl = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            googleMapUrl: value
        } 
    })
    handleChangeLocation = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            location: value
        } 
    })
    handleChangeUserId = (e, { value }) => this.setState({ 
        userId: value
    })


    handleChangeAirCondition = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                airCondition: !this.state.data.boolean.airCondition                                
            }}
        })
    }

    handleChangeFan = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                fan: !this.state.data.boolean.fan                                
            }}
        })
    }

    handleChangeProjector = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                projector: !this.state.data.boolean.projector                               
            }}
        })
    }

    handleChangeWhiteBoard = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                whiteBoard: !this.state.data.boolean.whiteBoard                             
            }}
        })
    }

    handleChangeBlackBoard = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                blackBoard: !this.state.data.boolean.blackBoard                               
            }}
        })
    }

    handleChangeAudioSystem = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                audioSystem: !this.state.data.boolean.audioSystem                          
            }}
        })
    }

    handleChangeComputer = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                computer: !this.state.data.boolean.computer                                
            }}
        })
    }

    handleChangeWifi = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                wifi: !this.state.data.boolean.wifi                                
            }}
        })
    }

    handleChangeRefrigerator = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                refrigerator: !this.state.data.boolean.refrigerator                                
            }}
        })
    }

    handleChangeKitchen = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                kitchen: !this.state.data.boolean.kitchen                                
            }}
        })
    }

    handleChangeMicrowave = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                microwave: !this.state.data.boolean.microwave                                
            }}
        })
    }

    handleChangeTelevistion = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                televistion: !this.state.data.boolean.televistion                                
            }}
        })
    }

    handleChangeFitness = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                fitness: !this.state.data.boolean.fitness                               
            }}
        })
    }

    handleChangePool = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                pool: !this.state.data.boolean.pool                                
            }}
        })
    }



    handleFileSubmit = (e) => {
        let files = e.target.files
        let reader = new FileReader()
        reader.readAsDataURL(files[0])
        reader.onload = (e)=>{
            let roomMainPicture = e.target.result
            console.log(roomMainPicture)
            this.setState({
                data: { ...this.state.data,
                    roomMainPicture
                },
            })
        }
    }

    dataHaveEmptyString = (data, exceptionItemList=[]) => {
        let isDataHaveEmptyString = false

        for (var key in data) {
            if(exceptionItemList.includes(key)) {
                continue
            }
            else if(data[key]==='') {
                isDataHaveEmptyString = true
                break
            }
        }
        return isDataHaveEmptyString
    }

    updateErrorRequireField = () => {
        let {roomTypeId} = this.state
        let {buildingName, roomName, costPerHour, costPerDay, capacity, province} = this.state.data

        this.setState({ requireField: { ...this.state.requireField,
            isEmptyRoomTypeId: roomTypeId==='',
            isEmptyBuildingName: buildingName==='',
            isEmptyRoomName: roomName==='',
            isEmptyCostPerHour: costPerHour==='',
            isEmptyCostPerDay: costPerDay==='',
            isEmptyCapacity: capacity==='',
            isEmptyProvince: province==='',
        }})
    }

    handleUpdateRoom = () => {
        let data = this.state.data
        let exceptionItemList = ['googleMapUrl', 'location']
        let {roomId, roomTypeId, userId} = this.state

        let condition = this.dataHaveEmptyString(data, exceptionItemList)
        this.updateErrorRequireField()
        
        if(condition) {
            this.openRequireModal()
        }
        else {
            data = {...data, ...data.boolean, roomId, roomTypeId, userId}
            console.log('data is ready for send')
            console.log(data)
            this.updateRoom(data)
        }
    }

    openRequireModal = () => {
        this.setState({ isRequireModalOpen: true })
    }

    closeRequireModal = () => {
        this.setState({ isRequireModalOpen: false })
    }

    renderRequireModal = () => {
        return (
            <Modal 
            centered={false}
            style={{ position: 'static', marginTop: '30vh' }}
            open={this.state.isRequireModalOpen}
            onClose={this.closeRequireModal} 
            basic 
            size='small'
            >
                <Header icon='remove' content='ข้อมูลไม่ครบถ้วน' style={{color:'indianred'}}/>
                <Modal.Content>
                <p>
                    กรุณากรอกข้อมูลให้ครบถ้วน ตามช่องที่มีเครื่องหมาย(*) 
                </p>
                </Modal.Content>
                <Modal.Actions>
                <Button basic color='green' inverted onClick={this.closeRequireModal}>
                    <Icon name='backward' /> Back
                </Button>
                </Modal.Actions>
            </Modal>
        )
        
    }

    renderFeatureZone = () => {
        let {roomTypeId} = this.state
        let data = this.state.data
        let {airCondition, fan, projector, whiteBoard, blackBoard, 
            audioSystem, computer, wifi, refrigerator, 
            microwave, televistion, fitness, pool} = data.boolean

        if(roomTypeId === 1) {
            return (
                <div>
                    <Form.Group widths='equal'>

                        <Form.Field>
                            <Checkbox 
                            label={{ children: 'airCondition' }} 
                            checked={airCondition}
                            onChange={() => this.handleChangeAirCondition()} 
                            />
                        </Form.Field>
                        <Form.Field>
                            <Checkbox 
                            label={{ children: 'พัดลม' }} 
                            checked={fan}
                            onChange={() => this.handleChangeFan()} 
                            />
                        </Form.Field>
                        <Form.Field>
                            <Checkbox 
                            label={{ children: 'projector' }} 
                            checked={projector}
                            onChange={() => this.handleChangeProjector()} 
                            />
                        </Form.Field>
                        <Form.Field>
                            <Checkbox 
                            label={{ children: 'whiteBoard' }}
                            checked={whiteBoard}
                            onChange={() => this.handleChangeWhiteBoard()} 
                             />
                        </Form.Field>
                        <Form.Field>
                            <Checkbox 
                            label={{ children: 'blackBoard' }} 
                            checked={blackBoard}
                            onChange={() => this.handleChangeBlackBoard()} 
                            />
                        </Form.Field>
                        <Form.Field>
                            <Checkbox 
                            label={{ children: 'audioSystem' }} 
                            checked={audioSystem}
                            onChange={() => this.handleChangeAudioSystem()} 
                            />
                        </Form.Field>
                        <Form.Field>
                            <Checkbox 
                            label={{ children: 'computer' }} 
                            checked={computer}
                            onChange={() => this.handleChangeComputer()} 
                            />
                        </Form.Field>
                        <Form.Field>
                            <Checkbox 
                            label={{ children: 'wifi' }} 
                            checked={wifi}
                            onChange={() => this.handleChangeWifi()} 
                            />
                        </Form.Field>

                    </Form.Group>
                </div>
            )
        }
        else if(roomTypeId === 2) {
            return (
                <div>
                    <Form.Group widths='equal'>

                    <Form.Field>
                        <Checkbox 
                        label={{ children: 'airCondition' }} 
                        checked={airCondition}
                        onChange={() => this.handleChangeAirCondition()} 
                        />
                    </Form.Field>
                    <Form.Field>
                        <Checkbox 
                        label={{ children: 'พัดลม' }} 
                        checked={fan}
                        onChange={() => this.handleChangeFan()} 
                        />
                    </Form.Field>
                    <Form.Field>
                        <Checkbox 
                        label={{ children: 'projector' }} 
                        checked={projector}
                        onChange={() => this.handleChangeProjector()} 
                        />
                    </Form.Field>
                    <Form.Field>
                        <Checkbox 
                        label={{ children: 'whiteBoard' }}
                        checked={whiteBoard}
                        onChange={() => this.handleChangeWhiteBoard()} 
                        />
                    </Form.Field>
                    <Form.Field>
                        <Checkbox 
                        label={{ children: 'blackBoard' }} 
                        checked={blackBoard}
                        onChange={() => this.handleChangeBlackBoard()} 
                        />
                    </Form.Field>
                    <Form.Field>
                        <Checkbox 
                        label={{ children: 'audioSystem' }} 
                        checked={audioSystem}
                        onChange={() => this.handleChangeAudioSystem()} 
                        />
                    </Form.Field>
                    <Form.Field>
                        <Checkbox 
                        label={{ children: 'computer' }} 
                        checked={computer}
                        onChange={() => this.handleChangeComputer()} 
                        />
                    </Form.Field>
                    <Form.Field>
                        <Checkbox 
                        label={{ children: 'wifi' }} 
                        checked={wifi}
                        onChange={() => this.handleChangeWifi()} 
                        />
                    </Form.Field>

                    </Form.Group>
                </div>
            )
        }
        else if(roomTypeId === 3) {
            return (
                <div>
                    <Form.Group widths='equal'>

                        <Form.Field>
                            <Checkbox 
                            label={{ children: 'airCondition' }} 
                            checked={airCondition}
                            onChange={() => this.handleChangeAirCondition()} 
                            />
                        </Form.Field>
                        <Form.Field>
                            <Checkbox 
                            label={{ children: 'พัดลม' }} 
                            checked={fan}
                            onChange={() => this.handleChangeFan()} 
                            />
                        </Form.Field>
                        <Form.Field>
                            <Checkbox 
                            label={{ children: 'refrigerator' }}
                            checked={refrigerator}
                            onChange={() => this.handleChangeRefrigerator()}  
                            />
                        </Form.Field>
                        <Form.Field>
                            <Checkbox 
                            label={{ children: 'microwave' }} 
                            checked={microwave}
                            onChange={() => this.handleChangeMicrowave()} 
                            />
                        </Form.Field>
                        <Form.Field>
                            <Checkbox 
                            label={{ children: 'televistion' }} 
                            checked={televistion}
                            onChange={() => this.handleChangeTelevistion()} 
                            />
                        </Form.Field>
                        <Form.Field>
                            <Checkbox 
                            label={{ children: 'wifi' }} 
                            checked={wifi}
                            onChange={() => this.handleChangeWifi()} 
                            />
                        </Form.Field>
                        <Form.Field>
                            <Checkbox 
                            label={{ children: 'fitness' }} 
                            checked={fitness}
                            onChange={() => this.handleChangeFitness()} 
                            />
                        </Form.Field>
                        <Form.Field>
                            <Checkbox 
                            label={{ children: 'pool' }} 
                            checked={pool}
                            onChange={() => this.handleChangePool()} 
                            />
                        </Form.Field>

                    </Form.Group>
                </div>
            )
        }
        else {return null}
    }

    render() {
        let {isLoading} = this.state
        let {responsePackageReceive, responsePackageAll} = this.state

        if(isLoading || responsePackageReceive < responsePackageAll) return <Spinner/>
        /////////////
        const {optionsProvince, optionsUser, roomTypeId, userId} = this.state
        let {roomMainPicture, buildingName, roomName, costPerHour, costPerDay, capacity
            , province,  googleMapUrl, location} = this.state.data
        let {isEmptyRoomTypeId, isEmptyBuildingName, isEmptyRoomName, isEmptyCostPerHour
            , isEmptyCostPerDay, isEmptyCapacity, isEmptyProvince} = this.state.requireField

        const roomTypeIdOptions = [
            {key: 'classRoom', text: 'ห้องเรียน', value: 1},
            {key: 'meetingRoom', text: 'ห้องประชุม', value: 2},
            {key: 'restRoom', text: 'ห้องพัก', value: 3}
        ]

        return (
            <div style={{margin:'0vw 10vw 0 10vw'}}>

                {this.renderRequireModal()}
                
                <Segment>
                <Form>

                    <Divider horizontal>
                    <Header as='h5'>
                        Room Detail
                    </Header>
                    </Divider>

                    <Form.Group widths='equal'>
                    <Form.Field
                        control={Input}
                        required

                        label='อาคาร'
                        placeholder='ชื่อ อาคาร'

                        value={buildingName}
                        error={isEmptyBuildingName}
                        onChange={this.handleChangeBuildingName}
                    />
                    <Form.Field
                        control={Input}
                        required

                        label='ห้อง'
                        placeholder='ชื่อ ห้อง'

                        value={roomName}
                        error={isEmptyRoomName}
                        onChange={this.handleChangeRoomName}
                    />
                    <Form.Field
                        control={Select}
                        required

                        label={{ children: 'ประเภทห้อง'}}
                        placeholder='ประเภททห้อง'
                        
                        options={roomTypeIdOptions}
                        defaultValue={roomTypeId}
                        value={roomTypeId}
                        error={isEmptyRoomTypeId}
                        onChange={this.handleChangeRoomTypeId}
                    />
                    </Form.Group>
                    
                    <Form.Group widths='equal'>
                    <Form.Field
                        control={Input}
                        required
                        
                        label='ราคา / ชั่วโมง'
                        placeholder='ราคา'

                        pattern="[0-9]*"
                        value={costPerHour}
                        error={isEmptyCostPerHour}
                        onChange={this.handleChangeCostPerHour}
                    />
                    <Form.Field
                        control={Input}
                        required

                        label='ราคา / วัน'
                        placeholder='ราคา'

                        pattern="[0-9]*"
                        value={costPerDay}
                        error={isEmptyCostPerDay}
                        onChange={this.handleChangeCostPerDay}
                    />
                    <Form.Field
                        control={Input}
                        required

                        label='ความจุคน'
                        placeholder='จำนวนคน'

                        pattern="[0-9]*"
                        value={capacity}
                        error={isEmptyCapacity}
                        onChange={this.handleChangeCapacity}
                    />
                    </Form.Group>

                    <Form.Group >
                    <Form.Field
                        width={6}
                        control={Select}
                        required

                        label={{ children: 'จังหวัด'}}
                        placeholder='จังหวัด'
                        search

                        options={optionsProvince}
                        value={province}
                        error={isEmptyProvince}
                        onChange={this.handleChangeProvince}
                    />
                    <Form.Field
                        width={10}
                        control={Input}

                        label='google map Url'
                        placeholder='google map Url'

                        value={googleMapUrl}
                        onChange={this.handleChangeGoogleMapUrl}
                    />
                    </Form.Group>

                    <Form.Group >
                    <Form.Field
                        width={10}
                        control={TextArea}

                        label='สถานที่'
                        placeholder='สถานที่'

                        value={location}
                        onChange={this.handleChangeLocation}
                    />  
                    <Form.Field width={1}/>
                    <Form.Field
                        width={5}
                        control={Select}

                        label={{ children: 'ผู้ใช้ห้อง'}}
                        placeholder='ชื่อผู้ใช้'
                        search

                        options={optionsUser}
                        value={userId}
                        onChange={this.handleChangeUserId}
                    />
                    </Form.Group>

                    <Divider horizontal style={{margin:'1.5rem 0'}}>
                    <Header as='h5'>
                        Room Feature
                    </Header>
                    </Divider>

                    {this.renderFeatureZone()}

                </Form>        
                </Segment>     

                <div style={{display:'flex', justifyContent:'space-between'}}>
                    <div>
                        <input type='file' name='file' onChange={this.handleFileSubmit} />
                        <Image src={roomMainPicture} style={{width:'260px', height:'225px'}} />
                    </div>
                    
                    <Button
                        style={{maxHeight:'42px'}}
                        size='large'
                        color='green'
                        attached='bottom'
                        content='Save'
                        onClick={this.handleUpdateRoom}
                    />
                </div>   

            </div>
        )
    }
}

export default withRouter(AdminEditRoom)