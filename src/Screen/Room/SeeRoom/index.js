// src/Component/Login/index.js
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import Spinner from '../../../Component/Loader'

import { Button, Grid, Image, Table } from 'semantic-ui-react'

import './style.css';

class SeeRoom extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,

            response: [],
            responsePackageReceive: 0,
            responsePackageAll: 0,
            
            data: {},
            /////////////
            roomId: this.props.match.params.roomId,
            room:{}
            
        }
    }

    componentDidMount() {
        this.setUp()
    }

    setUp = () => {
        let apiList = [
            () => this.getRoomByIdApi(this.state.roomId),
            () => this.searchApi({
                roomTypeId: this.state.room.roomTypeId,
                roomId: this.state.room.roomId 
            })
        ]
        this.callApiBySequence(apiList)
        // this.callApiByParallel(apiList)
    }

    getRoomByIdApi = (roomId) => {
        return this.fetchData(
            `http://localhost:5000/room/roomId/${roomId}`, 
            res => {
                this.setState({room: res.room})
                console.log(this.state.room)
            },
        )
    }
    searchApi = (body) => {
        return this.fetchData(
            'http://localhost:5000/room', 
            res => {
                this.setState({room: res.roomList[0]})
                console.log(this.state.room)
            },
            'POST',
            {...body}
        )
    }
/////////////this is function set/////////////

    callApiByParallel = (fetchFuncList) => {
        this.setState({
            responsePackageAll: fetchFuncList.length,
            isLoading: true
        })
        fetchFuncList.forEach( fetchFunc => {
            fetchFunc()
        })
        this.setState({
            isLoading: false
        })
    }

    callApiBySequence = (fetchFuncList, index=0) => {
        if(index === 0) {
            this.setState({
                responsePackageAll: fetchFuncList.length,
                isLoading: true
            })
        }
        if(!fetchFuncList[index]) {
            return this.setState({isLoading: false})
        }

        fetchFuncList[index]()
                .then( () => this.callApiBySequence(fetchFuncList, index+1))

    }

    fetchData = (url='/default', interactData=()=>{}, method='GET', data={}) => {
        return fetch(url, this.httpOption(method, data))
                .then(res => res.json())
                .then(res => {
                    interactData(res)
                    return res
                })
                .then( res => {
                    // let array = this.state.response
                    // array.push(res)
                    // this.setState({response: array})
                    this.setState({responsePackageReceive: this.state.responsePackageReceive+1})
                    return res
                })
    }

    httpOption = (method, data) => {
        let option = {
            method, // *GET, POST, PUT, DELETE, etc.
            // mode: "no-cors", // no-cors, cors, *same-origin
            // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
                // "ku-room-token": localStorage.getItem('ku-room-token')
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            // redirect: "follow", // manual, *follow, error
            // referrer: "no-referrer", // no-referrer, *client
        }

        const token = localStorage.getItem('ku-room-token')
        if(token) {
            option.headers['ku-room-token'] = token
        }

        let isMethodWithBody = false
        const methodWithBodyList = ['POST', 'PUT']
        methodWithBodyList.forEach( methodWithBody => {
            let logic = method === methodWithBody
            isMethodWithBody = isMethodWithBody || logic
        })
        if(isMethodWithBody) {
            option.body = JSON.stringify(data) // body data type must match "Content-Type" header
        }
        
        return option
    }

    redirect = (path='/', isReplacePageHistory=false) => {
        if(!isReplacePageHistory) this.props.history.push(path)
        else this.props.history.replace(path)
    }

/////////////this is function set/////////////

    buttonSet = (onClick=()=>{}, content='default') => {
        return (
            <div>
                <Button positive onClick={()=>onClick()}>
                    {content}
                </Button>
            </div>
        )
    }

    standardRender = () => {
        let {data} = this.state
        return (
            <div>
                {data[0].roomName}
                {this.buttonSet(
                    () => this.redirect(),
                    'redirect'
                )}
            </div>
        )
    }

    renderDataValue = () => {
        let {department, location, buildingName, roomName, capacity, costPerHour, costPerDay, campus, faculty} = this.state.room
        return (
            <Table basic='very' celled collapsing>
                <Table.Body>
                <Table.Row>
                    <Table.Cell>วิทยาเขต</Table.Cell>
                    <Table.Cell>{campus}</Table.Cell>
                </Table.Row>
                <Table.Row>
                    <Table.Cell>หน่วยงาน</Table.Cell>
                    <Table.Cell>{faculty}</Table.Cell>
                </Table.Row>
                <Table.Row>
                    <Table.Cell>ที่อยู่</Table.Cell>
                    <Table.Cell>{location}</Table.Cell>
                </Table.Row>
                <Table.Row>
                    <Table.Cell>ตึก</Table.Cell>
                    <Table.Cell>{buildingName}</Table.Cell>
                </Table.Row>
                <Table.Row>
                    <Table.Cell>ห้อง</Table.Cell>
                    <Table.Cell>{roomName}</Table.Cell>
                </Table.Row>
                <Table.Row>
                    <Table.Cell>ความจุ</Table.Cell>
                    <Table.Cell>{capacity} คน</Table.Cell>
                </Table.Row>
                <Table.Row>
                    <Table.Cell>ราคา/ชั่วโมง</Table.Cell>
                    <Table.Cell>{costPerHour} บาท</Table.Cell>
                </Table.Row>
                <Table.Row>
                    <Table.Cell>ราคา/วัน</Table.Cell>
                    <Table.Cell>{costPerDay} บาท</Table.Cell>
                </Table.Row>
                
                </Table.Body>
            </Table>
        )
    }

    renderDataFeature = () => {
        let {roomTypeId, airCondition, fan, projector, whiteBoard, blackBoard, 
            audioSystem, computer, wifi, refrigerator, microwave, televistion,
            fitness, pool} = this.state.room
        if(roomTypeId===1||roomTypeId===2) {
            return (
                <Table basic='very' celled collapsing>
                    <Table.Body>
                    <Table.Row>
                        <Table.Cell>เครื่องปรับอากาศ</Table.Cell>
                        <Table.Cell>{airCondition}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>พัดลม</Table.Cell>
                        <Table.Cell>{fan}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>เครื่องฉายภาพ</Table.Cell>
                        <Table.Cell>{projector}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>กระดานขาว</Table.Cell>
                        <Table.Cell>{whiteBoard}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>กระดานดำ</Table.Cell>
                        <Table.Cell>{blackBoard}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>เครื่องเสียง</Table.Cell>
                        <Table.Cell>{audioSystem}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>คอมพิวเตอร์</Table.Cell>
                        <Table.Cell>{computer}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>wifi</Table.Cell>
                        <Table.Cell>{wifi}</Table.Cell>
                    </Table.Row>
                    
                    </Table.Body>
                </Table>
            
            )
        }   
        else {
            return (
                <Table basic='very' celled collapsing>
                    <Table.Body>
                    <Table.Row>
                        <Table.Cell>เครื่องปรับอากาศ</Table.Cell>
                        <Table.Cell>{airCondition}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>พัดลม</Table.Cell>
                        <Table.Cell>{fan}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>ตู้เย็น</Table.Cell>
                        <Table.Cell>{refrigerator}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>ไมโครเวฟ</Table.Cell>
                        <Table.Cell>{microwave}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>โทรทัศน์</Table.Cell>
                        <Table.Cell>{televistion}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>wifi</Table.Cell>
                        <Table.Cell>{wifi}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>ฟิตเนส</Table.Cell>
                        <Table.Cell>{fitness}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>สระน้ำ</Table.Cell>
                        <Table.Cell>{pool}</Table.Cell>
                    </Table.Row>
                    
                    </Table.Body>
                </Table>
            )
        }
    }

    render() {
        let {isLoading} = this.state
        let {responsePackageReceive, responsePackageAll} = this.state

        if(isLoading || responsePackageReceive < responsePackageAll) return <Spinner/>
        /////////////

        let {roomMainPicture, telNo, email, facebook, lineId} = this.state.room

        return (
            <div style={{margin:'0vw 10vw 0 10vw'}}>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={8}>
                            <Image src={roomMainPicture} style={{width:'38vw', height:'46vh'}}/>
                            <div style={{margin:'0vw 0 0 4rem'}}>
                            <div>Tel     : {telNo}   </div>
                            <div>Email   : {email}   </div>
                            <div>Facebook: {facebook}</div>
                            <div>LineId  : {lineId}  </div>
                            </div>
                        </Grid.Column>
                        {/* <Grid.Column width={2}>
                        </Grid.Column> */}
                        <Grid.Column width={5}>
                            {this.renderDataValue()}
                        </Grid.Column>
                        <Grid.Column width={3}>
                            {this.renderDataFeature()}
                        </Grid.Column>
                        <Grid.Column width={1}>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }
}

export default withRouter(SeeRoom)