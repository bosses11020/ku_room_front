// src/Screen/Guest/index.js
import React, { Component } from 'react';
import './style.css';

class Guest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,

            roomList: {}
        }
    }

    componentDidMount() {
        this.prepareData()
    }

    prepareData = async () => {
        this.setState({isLoading: false})
    }

    render() {
        if(this.state.isLoading) return null

        return (
            <div>
                <h1>Guest</h1>
            </div>
        )
    }
}

export default Guest