// src/Screen/Search/ProviderSearch.js
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import Spinner from '../../../Component/Loader'

import { Button, Form, Segment, Input, Divider, Icon, Table, Select } from 'semantic-ui-react'
import {provinceList} from './province'

import './style.css';

class ProviderSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,

            response: {},
            responsePackageReceive: 0,
            responsePackageAll: 0,
            
            data: {
                userId: null,

                province: null,
                location: '',
                buildingName: '',
                roomName: ''
            },

            roomTypeId: 1,
            
            roomList: [],
            optionsProvince: []
            /////////////l
                    
            
        }
    }

    componentDidMount() {
        this.setup()
    }

    setup = () => {
        this.setState({
            startState: this.state
        })
        let optionsProvince = [{value: null, text: ''}]
        provinceList.forEach( province => {
            optionsProvince
            .push({
                value: province.name, 
                text: province.name
            })
        })

        this.setState({
            optionsProvince
        })

        let apiList = [
            () => this.getUserIdApi()
        ]
        this.callApiByParallel(apiList)
    }

    search = () => {
        let body = {}
        let data = this.state.data
        body = {
            ...body,
            ...data,
            roomTypeId: this.state.roomTypeId,
        }
        console.log(body)

        let apiList = [
            () => this.searchApi(body)
        ]

        this.callApiByParallel(apiList)

    }

    deleteRoom = (roomId) => {
        let body = {}
        let data = this.state.data
        body = {
            ...body,
            ...data,
            roomTypeId: this.state.roomTypeId,
        }
        let apiList = [
            () => this.daleteRoomApi(roomId),
            () => this.searchApi(body)
        ]

        // this.callApiByParallel(apiList)
        this.callApiBySequence(apiList)
    }

    searchApi = (body) => {
        return this.fetchData(
            'http://localhost:5000/room', 
            res => {
                this.setState({roomList: res.roomList})
                console.log(this.state.roomList)
            },
            'POST',
            {...body}
        )
    }

    daleteRoomApi = (roomId) => {
        return this.fetchData(
            `http://localhost:5000/room/delete/roomId/${roomId}`, 
            res => {
                console.log(res)
            },
            'POST'
        )
    }

    getUserIdApi = () => {
        return this.fetchData(
            `http://localhost:5000/token/userId`, 
            res => {
                this.setState({
                    data: {...this.state.data,
                        userId: res.userId    
                    }
                })                
            }
        )
    }

/////////////this is function set/////////////

    callApiByParallel = (fetchFuncList) => {
        this.setState({
            responsePackageAll: fetchFuncList.length,
            isLoading: true
        })
        fetchFuncList.forEach( fetchFunc => {
            fetchFunc()
        })
        this.setState({
            isLoading: false
        })
    }

    callApiBySequence = (fetchFuncList, index=0) => {
        if(index === 0) {
            this.setState({
                responsePackageAll: fetchFuncList.length,
                isLoading: true
            })
        }
        if(!fetchFuncList[index]) {
            return this.setState({isLoading: false})
        }

        fetchFuncList[index]()
                .then( () => this.callApiBySequence(fetchFuncList, index+1))

    }

    fetchData = (url='/default', interactData=()=>{}, method='GET', data={}) => {
        return fetch(url, this.httpOption(method, data))
                .then(res => res.json())
                .then(res => {
                    interactData(res)
                    return res
                })
                .then( res => {
                    // let array = this.state.response
                    // array.push(res)
                    // this.setState({response: array})
                    this.setState({responsePackageReceive: this.state.responsePackageReceive+1})
                    return res
                })
    }

    httpOption = (method, data) => {
        let option = {
            method, // *GET, POST, PUT, DELETE, etc.
            // mode: "no-cors", // no-cors, cors, *same-origin
            // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
                // "ku-room-token": localStorage.getItem('ku-room-token')
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            // redirect: "follow", // manual, *follow, error
            // referrer: "no-referrer", // no-referrer, *client
        }

        const token = localStorage.getItem('ku-room-token')
        if(token) {
            option.headers['ku-room-token'] = token
        }

        let isMethodWithBody = false
        const methodWithBodyList = ['POST', 'PUT']
        methodWithBodyList.forEach( methodWithBody => {
            let logic = method === methodWithBody
            isMethodWithBody = isMethodWithBody || logic
        })
        if(isMethodWithBody) {
            option.body = JSON.stringify(data) // body data type must match "Content-Type" header
        }
        
        return option
    }

    redirect = (path='/', isReplacePageHistory=false) => {
        if(!isReplacePageHistory) this.props.history.push(path)
        else this.props.history.replace(path)
    }

/////////////this is function set/////////////

    handleChangeProvince = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            province: value
        } 
    })
    handleChangeLocation = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            location: value
        } 
    })
    handleChangeBuildingName = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            buildingName: value
        } 
    })
    handleChangeRoomName = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            roomName: value
        } 
    })

  

/////////////render set zone/////////////

    buttonSet = (onClick=()=>{}, content='default') => {
        return (
            <div>
                <Button positive onClick={()=>onClick()}>
                    {content}
                </Button>
            </div>
        )
    }

    standardRender = () => {
        let {data} = this.state
        return (
            <div>
                {data[0].roomName}
                {this.buttonSet(
                    () => this.redirect(),
                    'redirect'
                )}
            </div>
        )
    }

    changeRoomTypeId = (roomTypeId) => {
        this.setState({
            roomTypeId: roomTypeId,
        })
    }

    showData = () => {
        console.log(this.state)
    }

    renderButtonRoomTypeZone = () => {
        return ( 
            <Button.Group widths='3'>
                {this.renderButtonRoomType(1)}
                {this.renderButtonRoomType(2)}
                {this.renderButtonRoomType(3)}
            </Button.Group>
        )
    }

    renderButtonRoomType = (roomTypeId) => {
        if(this.state.roomTypeId === roomTypeId) {
            return (
                <Button color='blue' onClick={()=>this.changeRoomTypeId(roomTypeId)}>
                    {this.getRoomTypeName(roomTypeId)}
                </Button>
            )
        }
        else {
            return (
                <Button basic color='blue' onClick={()=>this.changeRoomTypeId(roomTypeId)}>
                    {this.getRoomTypeName(roomTypeId)}
                </Button>
            )
        }
    }

    getRoomTypeName = (roomTypeId) => {
        switch(roomTypeId) {
            case 1:
                return 'ห้องเรียน'
                break;
            case 2:
                return 'ห้องประชุม'
                break;
            case 3:
                return 'ห้องพัก'
                break;
            default:
                console.log('roomTypeId error')
                return null
        }
    }



    renderRoomListCard = () => {
        let roomListHtml = []

        let {roomList} = this.state
        roomList.forEach( room => {
            roomListHtml.push(this.renderRoomCard(room))
        })

        return (
            <Table celled>
                <Table.Header>
                <Table.Row>
                    <Table.HeaderCell width={3}>อาคาร</Table.HeaderCell>
                    <Table.HeaderCell width={3}>ห้อง</Table.HeaderCell>
                    <Table.HeaderCell width={2}>จังหวัด</Table.HeaderCell>
                    <Table.HeaderCell width={6}>สถานที่</Table.HeaderCell>
                    <Table.HeaderCell width={1}>แก้ไข</Table.HeaderCell>
                    <Table.HeaderCell width={1}>ลบ</Table.HeaderCell>
                </Table.Row>
                </Table.Header>

                <Table.Body>

                    {roomListHtml}

                </Table.Body>
            </Table>
            
        )
    }

    renderRoomCard = (room) => {
        let {buildingName, roomName, province, location, roomId} = room
        return (
            <Table.Row>
                <Table.Cell>{buildingName}</Table.Cell>
                <Table.Cell>{roomName}</Table.Cell>
                <Table.Cell>{province}</Table.Cell>
                <Table.Cell>{location}</Table.Cell>
                <Table.Cell style={{textAlign: 'center'}}><Icon className='pointer' name='edit' onClick={()=>this.redirect(`/provider/room/roomId/${roomId}`)}/></Table.Cell>
                <Table.Cell style={{textAlign: 'center'}}><Icon className='pointer' name='delete' onClick={()=>this.deleteRoom(roomId)}/></Table.Cell>
            </Table.Row>
        )

    }

/////////////this is render zone/////////////

    render() {
        let {isLoading} = this.state
        let {responsePackageReceive, responsePackageAll} = this.state

        if(isLoading || responsePackageReceive < responsePackageAll) return <Spinner/>
        /////////////
        let {roomTypeId, optionsProvince} = this.state
        let {location, province, buildingName, roomName} = this.state.data

        return (
            <div>
                <div style={{margin:'0vw 14vw'}}>
                    
                    {this.renderButtonRoomTypeZone()}

                    <Segment >          
                        <Form>
                            <Form.Group widths='equal'>
                            <Form.Field>
                                <label>อาคาร</label>
                                <Input 
                                    fluid placeholder='อาคาร' 
                                    value={buildingName}
                                    onChange={this.handleChangeBuildingName}
                                />
                            </Form.Field>
                            <Form.Field>
                                <label>ห้อง</label>
                                <Input 
                                    fluid placeholder='ห้อง' 
                                    value={roomName}
                                    onChange={this.handleChangeRoomName}
                                />
                            </Form.Field>
                            <Form.Field
                                control={Select}
                                searchInput={{ id: 'form-select-control-gender' }}
                                label={{ children: 'จังหวัด', htmlFor: 'form-select-control-gender' }}
                                placeholder='จังหวัด'
                                search

                                options={optionsProvince} 
                                value={province}
                                onChange={this.handleChangeProvince}
                            />
                            <Form.Field>
                                <label>สถานที่</label>
                                <Input 
                                    fluid placeholder='สถานที่' 
                                    value={location}
                                    onChange={this.handleChangeLocation}
                                />
                            </Form.Field>
                            </Form.Group>
                            
                            
                            <Divider clearing />

                            <Button
                                style={{width:'100%'}}
                                color='blue'
                                attached='bottom'
                                content='Search'
                                onClick={this.search}
                            />

                        </Form>
                    </Segment>

                    {this.renderRoomListCard()}

                    {/* <Button
                        style={{width:'100%'}}
                        color='blue'
                        attached='bottom'
                        content='Test'
                        onClick={this.showData}
                    /> */}

                </div>
            </div>
        )
    }
}

export default withRouter(ProviderSearch)