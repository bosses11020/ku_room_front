// src/Screen/Search/GuestSearch.js
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import Spinner from '../../../Component/Loader'

import { Button, Form, Segment, Checkbox, Input, Grid, Image, Dropdown, Divider, Select } from 'semantic-ui-react'
import {provinceList} from './province'
import {campusList} from '../../../Common/department'

import './style.css';

class GuestSeach extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,

            response: {},
            responsePackageReceive: 0,
            responsePackageAll: 0,
            
            data: {
                campus: null,
                faculty: null,

                province: '',
                costUnit: 'costPerHour',
                capacityStart: '',
                capacityEnd: '',
                costStart: '',
                costEnd: '',
                boolean: {
                    airCondition: false, 
                    fan: false, 
                    projector: false, 
                    whiteBoard: false, 
                    blackBoard: false, 
                    audioSystem: false, 
                    computer: false, 
                    wifi: false, 
                    refrigerator: false, 
                    kitchen: false, 
                    microwave: false, 
                    televistion: false, 
                    fitness: false, 
                    pool: false
                }
            },

            roomTypeId: 1,
            
            roomList: [],
            optionsProvince: [],
            optionsCampus: [],
            optionsFacultyOfCampus: {}
            /////////////l
                    
            
        }
    }

    componentDidMount() {
        this.setup()
    }

    componentDidUpdate() {
        console.log(this.state)
    }

    setup = () => {
        this.setState({
            startState: this.state
        })
        let optionsProvince = [{value: null, text: ''}]
        provinceList.forEach( province => {
            optionsProvince
            .push({
                value: province.name, 
                text: province.name
            })
        })

        let optionsCampus = [{value: null, text: 'ไม่เลือก'}]
        let optionsFacultyOfCampus = { 'null': [{value: null, text: 'ไม่เลือก'}] }

        for(let i=0 ; i<campusList.length ; i++) {
            let campus = campusList[i].campus

            optionsCampus
            .push({
                value: campus,
                text: campus
            })

            let facultyList = campusList[i].facultyList
            console.log(facultyList)
            let optionsFaculty = [{value: null, text: 'ไม่เลือก'}]
            for(let j=0 ; j<facultyList.length ; j++) {
                optionsFaculty
                .push({
                    value: facultyList[j].faculty,
                    text: facultyList[j].faculty
                })
            }

            optionsFacultyOfCampus[`${campus}`] = optionsFaculty
        }

        this.setState({
            optionsProvince,
            optionsCampus,
            optionsFacultyOfCampus
        })
    }

    search = () => {
        let body = {}
        let data = this.state.data

        body = {
            ...data.boolean
        }
        for( var key in body ){
            if(!body[key])
            delete body[key]         // remove the property from the object
        }

        let {province, costUnit, capacityStart, capacityEnd, costStart, costEnd, campus, faculty} = data
        body = {
            ...body,
            roomTypeId: this.state.roomTypeId,
            province,
            capacityStart,
            capacityEnd,

            campus,
            faculty
        }
        if(costUnit === 'costPerDay') {
            body = {
                ...body,
                costPerDayStart: costStart,
                costPerDayEnd: costEnd
            }
        }
        else { //costPerHour or anything default mean costPerHour
            body = {
                ...body,
                costPerHourStart: costStart,
                costPerHourEnd: costEnd
            }
        }
        for( var key in body ){
            if(body[key] === '')
            delete body[key]         // remove the property from the object
        }

        console.log(body)

        let apiList = [
            () => this.searchApi(body)
        ]

        this.callApiByParallel(apiList)

    }

    searchApi = (body) => {
        return this.fetchData(
            'http://localhost:5000/room', 
            res => {
                this.setState({roomList: res.roomList})
                console.log(this.state.roomList)
            },
            'POST',
            {...body}
        )
    }

    // login = () => {
    //     let {data} = this.state
    //     console.log(data)
    //     return this.fetchData(
    //                 'http://localhost:5000/login', 
    //                 res => {
    //                     console.log(res)
    //                     if(res.token) {
    //                         localStorage.setItem('ku-room-token', res.token)
    //                     }
    //                 },
    //                 'POST',
    //                 {...data}
    //             )
    // }

/////////////this is function set/////////////

    callApiByParallel = (fetchFuncList) => {
        this.setState({
            responsePackageAll: fetchFuncList.length,
            isLoading: true
        })
        fetchFuncList.forEach( fetchFunc => {
            fetchFunc()
        })
        this.setState({
            isLoading: false
        })
    }

    callApiBySequence = (fetchFuncList, index=0) => {
        if(index === 0) {
            this.setState({
                responsePackageAll: fetchFuncList.length,
                isLoading: true
            })
        }
        if(!fetchFuncList[index]) {
            return this.setState({isLoading: false})
        }

        fetchFuncList[index]()
                .then( () => this.callApiBySequence(fetchFuncList, index+1))

    }

    fetchData = (url='/default', interactData=()=>{}, method='GET', data={}) => {
        return fetch(url, this.httpOption(method, data))
                .then(res => res.json())
                .then(res => {
                    interactData(res)
                    return res
                })
                .then( res => {
                    // let array = this.state.response
                    // array.push(res)
                    // this.setState({response: array})
                    this.setState({responsePackageReceive: this.state.responsePackageReceive+1})
                    return res
                })
    }

    httpOption = (method, data) => {
        let option = {
            method, // *GET, POST, PUT, DELETE, etc.
            // mode: "no-cors", // no-cors, cors, *same-origin
            // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
                // "ku-room-token": localStorage.getItem('ku-room-token')
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            // redirect: "follow", // manual, *follow, error
            // referrer: "no-referrer", // no-referrer, *client
        }

        const token = localStorage.getItem('ku-room-token')
        if(token) {
            option.headers['ku-room-token'] = token
        }

        let isMethodWithBody = false
        const methodWithBodyList = ['POST', 'PUT']
        methodWithBodyList.forEach( methodWithBody => {
            let logic = method === methodWithBody
            isMethodWithBody = isMethodWithBody || logic
        })
        if(isMethodWithBody) {
            option.body = JSON.stringify(data) // body data type must match "Content-Type" header
        }
        
        return option
    }

    redirect = (path='/', isReplacePageHistory=false) => {
        if(!isReplacePageHistory) this.props.history.push(path)
        else this.props.history.replace(path)
    }

/////////////this is function set/////////////

    buttonSet = (onClick=()=>{}, content='default') => {
        return (
            <div>
                <Button positive onClick={()=>onClick()}>
                    {content}
                </Button>
            </div>
        )
    }

    standardRender = () => {
        let {data} = this.state
        return (
            <div>
                {data[0].roomName}
                {this.buttonSet(
                    () => this.redirect(),
                    'redirect'
                )}
            </div>
        )
    }

    handleChangeCampus = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            campus: value,
            faculty: null
        } 
    })
    handleChangeFaculty = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            faculty: value
        } 
    })

    handleChangeCapacityStart = (e) => {
        if(e.target.validity.valid && (e.target.value!=='0'||this.state.data.capacityStart!=='')) {
            this.setState({
                data: {
                    ...this.state.data,
                    capacityStart: e.target.value
                }
            })
        }
    }

    handleChangeCapacityEnd = (e) => {
        if(e.target.validity.valid && (e.target.value!=='0'||this.state.data.capacityEnd!=='')) {
            this.setState({
                data: {
                    ...this.state.data,
                    capacityEnd: e.target.value
                }
            })
        }
    }

    handleChangeCostStart = (e) => {
        if(e.target.validity.valid && (e.target.value!=='0'||this.state.data.costStart!=='')) {
            this.setState({
                data: {
                    ...this.state.data,
                    costStart: e.target.value
                }
            })
        }
    }

    handleChangeCostEnd = (e) => {
        if(e.target.validity.valid && (e.target.value!=='0'||this.state.data.costEnd!=='')) {
            this.setState({
                data: {
                    ...this.state.data,
                    costEnd: e.target.value
                }
            })
        }
    }

    handleChangeProvince = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            province: value
        } 
    })

    handleChangeCostUnit = (e, { value }) => this.setState({ 
        data:{
            ...this.state.data, 
            costUnit: value
        } 
    })

    handleChangeAirCondition = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                airCondition: !this.state.data.boolean.airCondition                                
            }}
        })
    }

    handleChangeFan = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                fan: !this.state.data.boolean.fan                                
            }}
        })
    }

    handleChangeProjector = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                projector: !this.state.data.boolean.projector                               
            }}
        })
    }

    handleChangeWhiteBoard = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                whiteBoard: !this.state.data.boolean.whiteBoard                             
            }}
        })
    }

    handleChangeBlackBoard = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                blackBoard: !this.state.data.boolean.blackBoard                               
            }}
        })
    }

    handleChangeAudioSystem = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                audioSystem: !this.state.data.boolean.audioSystem                          
            }}
        })
    }

    handleChangeComputer = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                computer: !this.state.data.boolean.computer                                
            }}
        })
    }

    handleChangeWifi = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                wifi: !this.state.data.boolean.wifi                                
            }}
        })
    }

    handleChangeRefrigerator = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                refrigerator: !this.state.data.boolean.refrigerator                                
            }}
        })
    }

    handleChangeKitchen = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                kitchen: !this.state.data.boolean.kitchen                                
            }}
        })
    }

    handleChangeMicrowave = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                microwave: !this.state.data.boolean.microwave                                
            }}
        })
    }

    handleChangeTelevistion = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                televistion: !this.state.data.boolean.televistion                                
            }}
        })
    }

    handleChangeFitness = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                fitness: !this.state.data.boolean.televistion                                
            }}
        })
    }

    handleChangePool = () => {
        this.setState({
            data:{...this.state.data, boolean: {
                ...this.state.data.boolean,
                pool: !this.state.data.boolean.pool                                
            }}
        })
    }

    changeRoomTypeId = (roomTypeId) => {
        this.setState({
            roomTypeId: roomTypeId,
            data: {
                ...this.state.startState.data,
                costUnit: roomTypeId===3 ? 'costPerDay' : 'costPerHour'
            }
        })
    }

    showData = () => {
        console.log(this.state)
    }

    renderButtonRoomTypeZone = () => {
        return ( 
            <Button.Group widths='3'>
                {this.renderButtonRoomType(1)}
                {this.renderButtonRoomType(2)}
                {this.renderButtonRoomType(3)}
            </Button.Group>
        )
    }

    renderButtonRoomType = (roomTypeId) => {
        if(this.state.roomTypeId === roomTypeId) {
            return (
                <Button color='blue' onClick={()=>this.changeRoomTypeId(roomTypeId)}>
                    {this.getRoomTypeName(roomTypeId)}
                </Button>
            )
        }
        else {
            return (
                <Button basic color='blue' onClick={()=>this.changeRoomTypeId(roomTypeId)}>
                    {this.getRoomTypeName(roomTypeId)}
                </Button>
            )
        }
    }

    getRoomTypeName = (roomTypeId) => {
        switch(roomTypeId) {
            case 1:
                return 'ห้องเรียน'
                break;
            case 2:
                return 'ห้องประชุม'
                break;
            case 3:
                return 'ห้องพัก'
                break;
            default:
                console.log('roomTypeId error')
                return null
        }
    }

    renderFeatureZone = (roomTypeId) => {
        let data = this.state.data
        let {airCondition, fan, projector, whiteBoard, blackBoard, 
            audioSystem, computer, wifi, refrigerator, 
            microwave, televistion, fitness, pool} = data.boolean
        if(roomTypeId === 1 || roomTypeId === 2)
            return (
                <Form.Group widths='3' inline style={{margin:'0 0 4vh 6vw'}}>
                    <Grid columns='four' divided>
                        <Grid.Row style={{paddingTop: '0.5rem', paddingBottom: '0.5rem'}}>
                        <Grid.Column>
                            <Checkbox 
                            label={{ children: 'airCondition' }} 
                            checked={airCondition}
                            onChange={() => this.handleChangeAirCondition()} 
                            />
                        </Grid.Column>
                        <Grid.Column>
                            <Checkbox 
                            label={{ children: 'พัดลม' }} 
                            checked={fan}
                            onChange={() => this.handleChangeFan()} 
                            />
                        </Grid.Column>
                        <Grid.Column>
                            <Checkbox 
                            label={{ children: 'projector' }} 
                            checked={projector}
                            onChange={() => this.handleChangeProjector()} 
                            />
                        </Grid.Column>
                        <Grid.Column>
                            <Checkbox 
                            label={{ children: 'whiteBoard' }}
                            checked={whiteBoard}
                            onChange={() => this.handleChangeWhiteBoard()} 
                             />
                        </Grid.Column>
                        </Grid.Row>

                        <Grid.Row style={{paddingTop: '0.5rem', paddingBottom: '0.5rem'}}>
                        <Grid.Column>
                            <Checkbox 
                            label={{ children: 'blackBoard' }} 
                            checked={blackBoard}
                            onChange={() => this.handleChangeBlackBoard()} 
                            />
                        </Grid.Column>
                        <Grid.Column>
                            <Checkbox 
                            label={{ children: 'audioSystem' }} 
                            checked={audioSystem}
                            onChange={() => this.handleChangeAudioSystem()} 
                            />
                        </Grid.Column>
                        <Grid.Column>
                            <Checkbox 
                            label={{ children: 'computer' }} 
                            checked={computer}
                            onChange={() => this.handleChangeComputer()} 
                            />
                        </Grid.Column>
                        <Grid.Column>
                            <Checkbox 
                            label={{ children: 'wifi' }} 
                            checked={wifi}
                            onChange={() => this.handleChangeWifi()} 
                            />
                        </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Form.Group>
            )
        else if(roomTypeId === 3)
            return (
                <Form.Group widths='3' inline style={{margin:'0 0 4vh 6vw'}}>
                    <Grid columns='four' divided>
                        <Grid.Row style={{paddingTop: '0.5rem', paddingBottom: '0.5rem'}}>
                        <Grid.Column>
                            <Checkbox 
                            label={{ children: 'airCondition' }} 
                            checked={airCondition}
                            onChange={() => this.handleChangeAirCondition()} 
                            />
                        </Grid.Column>
                        <Grid.Column>
                            <Checkbox 
                            label={{ children: 'พัดลม' }} 
                            checked={fan}
                            onChange={() => this.handleChangeFan()} 
                            />
                        </Grid.Column>
                        <Grid.Column>
                            <Checkbox 
                            label={{ children: 'refrigerator' }}
                            checked={refrigerator}
                            onChange={() => this.handleChangeRefrigerator()}  
                            />
                        </Grid.Column>
                        <Grid.Column>
                            <Checkbox 
                            label={{ children: 'microwave' }} 
                            checked={microwave}
                            onChange={() => this.handleChangeMicrowave()} 
                            />
                        </Grid.Column>
                        </Grid.Row>

                        <Grid.Row style={{paddingTop: '0.5rem', paddingBottom: '0.5rem'}}>
                        <Grid.Column>
                            <Checkbox 
                            label={{ children: 'televistion' }} 
                            checked={televistion}
                            onChange={() => this.handleChangeTelevistion()} 
                            />
                        </Grid.Column>
                        <Grid.Column>
                            <Checkbox 
                            label={{ children: 'wifi' }} 
                            checked={wifi}
                            onChange={() => this.handleChangeWifi()} 
                            />
                        </Grid.Column>
                        <Grid.Column>
                            <Checkbox 
                            label={{ children: 'fitness' }} 
                            checked={fitness}
                            onChange={() => this.handleChangeFitness()} 
                            />
                        </Grid.Column>
                        <Grid.Column>
                            <Checkbox 
                            label={{ children: 'pool' }} 
                            checked={pool}
                            onChange={() => this.handleChangePool()} 
                            />
                        </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Form.Group>
            )
        else {
            return (
                roomTypeId
            )
        }
    }

    renderRoomListCard = () => {
        let roomListHtml = []

        let {roomList} = this.state
        roomList.forEach( room => {
            roomListHtml.push(this.renderRoomCard(room))
        })

        return roomListHtml
    }

    renderRoomCard = (room) => {
        let {department, roomName, buildingName, capacity , costPerHour, costPerDay
            , picture, roomMainPicture, roomId} = room
        let {costUnit} = this.state.data
        return (
            <Segment >
                <Grid >
                <Grid.Row >
                    <Grid.Column width={4}>
                    <Image style={{width:'20vw', height:'25vh'}} src={roomMainPicture} />
                    </Grid.Column>
                    <Grid.Column width={9} style={{paddingRight:'28px'}}>
                        <div>{department}</div>
                        <div>ตึก: {buildingName}</div>
                        <div>ห้อง: {roomName}</div>
                        <div>รองรับจำนวน: {capacity} คน</div>
                        <div>
                            {costUnit==='costPerHour' ? 
                                `ราคา ${costPerHour} บาท/ชั่วโมง` :
                                `ราคา ${costPerDay} บาท/วัน`
                            }
                        </div>
                    </Grid.Column>
                    <Grid.Column width={3}>
                    <Button
                        style={{width:'100%', position:'absolute', bottom:'0', right:'14px'}}
                        color='blue'
                        attached='bottom'
                        content='detail'
                        onClick={()=>this.redirect(`/guest/room/roomId/${roomId}`)}
                    />
                    </Grid.Column>
                </Grid.Row>
                </Grid>
            </Segment>

        )

    }

/////////////this is render zone/////////////

    render() {
        let {isLoading} = this.state
        let {responsePackageReceive, responsePackageAll} = this.state

        if(isLoading || responsePackageReceive < responsePackageAll) return <Spinner/>
        /////////////
        let {province, costUnit, capacityStart, capacityEnd, costStart, costEnd, campus, faculty} = this.state.data
        let {roomTypeId} = this.state
        
        const {optionsProvince} = this.state
        const optionsCostUnit = [
            { value: 'costPerHour', text: 'บาท/ชม' },
            { value: 'costPerDay', text: 'บาท/วัน' },
        ]

        let {optionsCampus, optionsFacultyOfCampus} = this.state
        let optionsFaculty = optionsFacultyOfCampus[`${campus}`]

        return (
            <div>
                <div style={{margin:'0vw 14vw'}}>
                    
                    {this.renderButtonRoomTypeZone()}

                    <Segment placeholder>          
                        <Form>
                        
                            <Form.Group inline>
                                <Form.Field width={2}>
                                    <Checkbox className='form-checkbox' label={{ children: 'Capacity' }} />
                                </Form.Field>
                                <Form.Field className='shift-right' width={2}>
                                    <label>อย่างน้อย</label>
                                </Form.Field>
                                <Form.Field width={3}>
                                    <Input 
                                    placeholder='number' 
                                    pattern="[0-9]*"
                                    onChange={this.handleChangeCapacityStart}
                                    value={capacityStart}
                                    />
                                </Form.Field>
                                <Form.Field className='shift-right' width={2}>
                                    <label>ไม่เกิน</label>
                                </Form.Field>
                                <Form.Field width={3}>
                                    <Input 
                                    placeholder='number' 
                                    pattern="[0-9]*"
                                    onChange={this.handleChangeCapacityEnd}
                                    value={capacityEnd}
                                    />
                                </Form.Field>
                                <Form.Field className='shift-right' width={2}>
                                    <label>หน่วย</label>
                                </Form.Field>
                                <Form.Field className='shift-center' width={2}>
                                    <label>คน</label>
                                </Form.Field>
                            </Form.Group>

                            <Form.Group inline>
                                <Form.Field width={2}>
                                    <Checkbox className='form-checkbox' label={{ children: 'Price' }} />
                                </Form.Field>
                                    <Form.Field className='shift-right' width={2}>
                                    <label>อย่างน้อย</label>
                                </Form.Field>
                                <Form.Field width={3}>
                                    <Input 
                                    placeholder='number'
                                    pattern="[0-9]*"
                                    onChange={this.handleChangeCostStart}
                                    value={costStart}
                                    />
                                </Form.Field>
                                <Form.Field className='shift-right' width={2}>
                                    <label>ไม่เกิน</label>
                                </Form.Field>
                                <Form.Field width={3}>
                                    <Input 
                                    placeholder='number' 
                                    pattern="[0-9]*"
                                    onChange={this.handleChangeCostEnd}
                                    value={costEnd}
                                    />
                                </Form.Field>
                                <Form.Field className='shift-right' width={2}>
                                    <label>หน่วย</label>
                                </Form.Field>
                                <Form.Field width={2}>
                                    <Dropdown 
                                    placeholder='Select Subject'
                                    name="subject"
                                    onChange={this.handleChangeCostUnit}
                                    fluid
                                    selection
                                    options={optionsCostUnit} 
                                    value={costUnit}
                                    />
                                </Form.Field>
                            </Form.Group>
                            
                            <Form.Group inline>
                                <Form.Field width={2}>
                                    <Checkbox className='form-checkbox' label={{ children: 'Province' }} />
                                </Form.Field>
                                <Form.Field width={4}>
                                    <Dropdown 
                                    placeholder='จังหวัด'
                                    name="province"
                                    onChange={this.handleChangeProvince}
                                    search
                                    selection
                                    options={optionsProvince} 
                                    value={province}
                                    />
                                </Form.Field>
                                <Form.Field className='shift-right' width={2}>
                                    <label>หน่วยงาน</label>
                                </Form.Field>
                                <Form.Field
                                    width={4}
                                    control={Select}
                                    searchInput={{ id: 'form-select-control-campus' }}
                                    // label={{ children: 'วิทยาเขต', htmlFor: 'form-select-control-campus' }}
                                    placeholder='วิทยาเขต'
                                    search

                                    options={optionsCampus} 
                                    value={campus}
                                    onChange={this.handleChangeCampus}
                                />
                                <Form.Field
                                    width={4}
                                    control={Select}
                                    searchInput={{ id: 'form-select-control-faculty' }}
                                    // label={{ children: 'คณะ หน่วยงาน', htmlFor: 'form-select-control-faculty' }}
                                    placeholder='คณะ หน่วยงาน'
                                    search

                                    options={optionsFaculty} 
                                    value={faculty}
                                    onChange={this.handleChangeFaculty}
                                />
                            </Form.Group>

                            <Form.Group inline style={{marginBottom: '2em'}}>
                                <Form.Field width={2}>
                                    <Checkbox className='form-checkbox' label={{ children: 'Feature' }} />
                                </Form.Field>
                            </Form.Group>

                            {this.renderFeatureZone(roomTypeId)}

                            <Divider clearing />

                            <Button
                                style={{width:'100%'}}
                                color='blue'
                                attached='bottom'
                                content='Search'
                                onClick={this.search}
                            />

                        </Form>
                    </Segment>

                    {this.renderRoomListCard()}

                </div>
            </div>
        )
    }
}

export default withRouter(GuestSeach)