// src/Screen/Search/Admin/room.js
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import Spinner from '../../../Component/Loader'

import { Button, Form, Segment, Input, Divider, Icon, Table, Select } from 'semantic-ui-react'
import {campusList} from '../../../Common/department'

import './style.css';

class AdminUserSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,

            response: {},
            responsePackageReceive: 0,
            responsePackageAll: 0,
            
            data: {
                username: '',
                campus: null,
                faculty: null,
            },

            userList: [],

            optionsCampus: [],
            optionsFacultyOfCampus: {}
            /////////////l
                    
            
        }
    }

    componentDidMount() {
        this.setup()
    }

    componentDidUpdate() {
        console.log(this.state)
    }

    setup = () => {
        this.setState({
            startState: this.state
        })
        
        let optionsCampus = [{value: null, text: 'ไม่เลือก'}]
        let optionsFacultyOfCampus = { 'null': [{value: null, text: 'ไม่เลือก'}] }

        for(let i=0 ; i<campusList.length ; i++) {
            let campus = campusList[i].campus

            optionsCampus
            .push({
                value: campus,
                text: campus
            })

            let facultyList = campusList[i].facultyList
            let optionsFaculty = [{value: null, text: 'ไม่เลือก'}]
            for(let j=0 ; j<facultyList.length ; j++) {
                optionsFaculty
                .push({
                    value: facultyList[j].faculty,
                    text: facultyList[j].faculty
                })
            }

            optionsFacultyOfCampus[`${campus}`] = optionsFaculty
        }

        this.setState({
            optionsCampus,
            optionsFacultyOfCampus
        })

    }

    search = () => {
        let body = {}
        let data = this.state.data

        body = {
            ...body,
            ...data,
        }

        let apiList = [
            () => this.searchApi(body)
        ]

        this.callApiByParallel(apiList)

    }

    deleteUser = (userId) => {
        let body = {}
        let data = this.state.data
        body = {
            ...body,
            ...data
        }
        let apiList = [
            () => this.daleteUserApi(userId),
            () => this.searchApi(body)
        ]

        // this.callApiByParallel(apiList)
        this.callApiBySequence(apiList)
    }

    searchApi = (body) => {
        return this.fetchData(
            'http://localhost:5000/user/search', 
            res => {
                this.setState({userList: res.userList})
                console.log(res)
            },
            'POST',
            {...body}
        )
    }

    daleteUserApi = (userId) => {
        return this.fetchData(
            `http://localhost:5000/user/delete/userId/${userId}`, 
            res => {
                console.log(res)
            },
            'POST'
        )
    }

/////////////this is function set/////////////

    callApiByParallel = (fetchFuncList) => {
        this.setState({
            responsePackageAll: fetchFuncList.length,
            isLoading: true
        })
        fetchFuncList.forEach( fetchFunc => {
            fetchFunc()
        })
        this.setState({
            isLoading: false
        })
    }

    callApiBySequence = (fetchFuncList, index=0) => {
        if(index === 0) {
            this.setState({
                responsePackageAll: fetchFuncList.length,
                isLoading: true
            })
        }
        if(!fetchFuncList[index]) {
            return this.setState({isLoading: false})
        }

        fetchFuncList[index]()
                .then( () => this.callApiBySequence(fetchFuncList, index+1))

    }

    fetchData = (url='/default', interactData=()=>{}, method='GET', data={}) => {
        return fetch(url, this.httpOption(method, data))
                .then(res => res.json())
                .then(res => {
                    interactData(res)
                    return res
                })
                .then( res => {
                    // let array = this.state.response
                    // array.push(res)
                    // this.setState({response: array})
                    this.setState({responsePackageReceive: this.state.responsePackageReceive+1})
                    return res
                })
    }

    httpOption = (method, data) => {
        let option = {
            method, // *GET, POST, PUT, DELETE, etc.
            // mode: "no-cors", // no-cors, cors, *same-origin
            // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
                // "ku-room-token": localStorage.getItem('ku-room-token')
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            // redirect: "follow", // manual, *follow, error
            // referrer: "no-referrer", // no-referrer, *client
        }

        const token = localStorage.getItem('ku-room-token')
        if(token) {
            option.headers['ku-room-token'] = token
        }

        let isMethodWithBody = false
        const methodWithBodyList = ['POST', 'PUT']
        methodWithBodyList.forEach( methodWithBody => {
            let logic = method === methodWithBody
            isMethodWithBody = isMethodWithBody || logic
        })
        if(isMethodWithBody) {
            option.body = JSON.stringify(data) // body data type must match "Content-Type" header
        }
        
        return option
    }

    redirect = (path='/', isReplacePageHistory=false) => {
        if(!isReplacePageHistory) this.props.history.push(path)
        else this.props.history.replace(path)
    }

/////////////this is function set/////////////

    handleChangeUsername = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            username: value
        } 
    })
    handleChangeCampus = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            campus: value,
            faculty: null
        } 
    })
    handleChangeFaculty = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            faculty: value
        } 
    })

/////////////render set zone/////////////

    buttonSet = (onClick=()=>{}, content='default') => {
        return (
            <div>
                <Button positive onClick={()=>onClick()}>
                    {content}
                </Button>
            </div>
        )
    }

    standardRender = () => {
        let {data} = this.state
        return (
            <div>
                {data[0].roomName}
                {this.buttonSet(
                    () => this.redirect(),
                    'redirect'
                )}
            </div>
        )
    }

    showData = () => {
        console.log(this.state)
    }

    renderUserListCard = () => {
        let userListHtml = []

        let {userList} = this.state
        userList.forEach( user => {
            userListHtml.push(this.renderUserCard(user))
        })

        return (
            <Table celled>
                <Table.Header>
                <Table.Row>
                    <Table.HeaderCell width={1}>Id</Table.HeaderCell>
                    <Table.HeaderCell width={3}>username</Table.HeaderCell>
                    <Table.HeaderCell width={3}>วิทยาเขต</Table.HeaderCell>
                    <Table.HeaderCell width={3}>คณะ หน่วยงาน</Table.HeaderCell>
                    <Table.HeaderCell width={2}>เบอร์โทรศัพท์</Table.HeaderCell>
                    <Table.HeaderCell width={2}>อีเมล</Table.HeaderCell>
                    <Table.HeaderCell width={1}>แก้ไข</Table.HeaderCell>
                    <Table.HeaderCell width={1}>ลบ</Table.HeaderCell>
                </Table.Row>
                </Table.Header>

                <Table.Body>

                    {userListHtml}

                </Table.Body>
            </Table>
            
        )
    }

    renderUserCard = (user) => {
        let {id, username, campus, faculty, telNo, email} = user
        let userId = id
        return (
            <Table.Row>
                <Table.Cell>{userId}</Table.Cell>
                <Table.Cell>{username}</Table.Cell>
                <Table.Cell>{campus}</Table.Cell>
                <Table.Cell>{faculty}</Table.Cell>
                <Table.Cell>{telNo}</Table.Cell>
                <Table.Cell>{email}</Table.Cell>
                <Table.Cell style={{textAlign: 'center'}}>
                    <Icon className='pointer' name='edit' onClick={()=>this.redirect(`/admin/user/userId/${userId}`)}/>
                </Table.Cell>
                <Table.Cell style={{textAlign: 'center'}}>
                    <Icon className='pointer' name='delete' onClick={()=>this.deleteUser(userId)} />
                </Table.Cell>

                {/* onClick={()=>this.redirect(`/admin/room/roomId/${roomId}`)}
                onClick={()=>this.deleteRoom(userId)} */}
            </Table.Row>
        )

    }

/////////////this is render zone/////////////

    render() {
        let {isLoading} = this.state
        let {responsePackageReceive, responsePackageAll} = this.state

        if(isLoading || responsePackageReceive < responsePackageAll) return <Spinner/>
        /////////////
        let {optionsCampus, optionsFacultyOfCampus} = this.state
        let {username, campus, faculty} = this.state.data
        
        let optionsFaculty = optionsFacultyOfCampus[`${campus}`]

        return (
            <div>
                <div style={{margin:'0vw 14vw'}}>
                    
                    <Segment >          
                        <Form>

                            <Form.Group widths='equal'>
                            <Form.Field>
                                <label>เจ้าของห้อง</label>
                                <Input 
                                    fluid placeholder='username' 
                                    value={username}
                                    onChange={this.handleChangeUsername}
                                />
                            </Form.Field>
                            <Form.Field
                                control={Select}
                                searchInput={{ id: 'form-select-control-campus' }}
                                label={{ children: 'วิทยาเขต', htmlFor: 'form-select-control-campus' }}
                                placeholder='วิทยาเขต'
                                search

                                options={optionsCampus} 
                                value={campus}
                                onChange={this.handleChangeCampus}
                            />
                            <Form.Field
                                control={Select}
                                searchInput={{ id: 'form-select-control-faculty' }}
                                label={{ children: 'คณะ หน่วยงาน', htmlFor: 'form-select-control-faculty' }}
                                placeholder='คณะ หน่วยงาน'
                                search

                                options={optionsFaculty} 
                                value={faculty}
                                onChange={this.handleChangeFaculty}
                            />
                            </Form.Group>

                            <Divider clearing />

                            <Button
                                style={{width:'100%'}}
                                color='blue'
                                attached='bottom'
                                content='Search'
                                onClick={this.search}
                            />

                        </Form>
                    </Segment>

                    {this.renderUserListCard()}

                    {/* <Button
                        style={{width:'100%'}}
                        color='blue'
                        attached='bottom'
                        content='Test'
                        onClick={this.showData}
                    /> */}

                </div>
            </div>
        )
    }
}

export default withRouter(AdminUserSearch)