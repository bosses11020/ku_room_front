// src/Screen/Search/Admin/room.js
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import Spinner from '../../../Component/Loader'

import { Button, Form, Segment, Input, Divider, Icon, Table, Select } from 'semantic-ui-react'
import {provinceList} from './province'
import {campusList} from '../../../Common/department'

import './style.css';

class AdminRoomSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,

            response: {},
            responsePackageReceive: 0,
            responsePackageAll: 0,
            
            data: {
                campus: null,
                faculty: null,

                username: '',
                province: null,
                location: '',
                buildingName: '',
                roomName: ''
            },

            roomTypeId: 1,
            
            roomList: [],
            optionsProvince: [],
            optionsCampus: [],
            optionsFacultyOfCampus: {}
            /////////////l
                    
            
        }
    }

    componentDidMount() {
        this.setup()
    }

    setup = () => {
        this.setState({
            startState: this.state
        })
        let optionsProvince = [{value: null, text: ''}]
        provinceList.forEach( province => {
            optionsProvince
            .push({
                value: province.name, 
                text: province.name
            })
        })

        let optionsCampus = [{value: null, text: 'ไม่เลือก'}]
        let optionsFacultyOfCampus = { 'null': [{value: null, text: 'ไม่เลือก'}] }

        for(let i=0 ; i<campusList.length ; i++) {
            let campus = campusList[i].campus

            optionsCampus
            .push({
                value: campus,
                text: campus
            })

            let facultyList = campusList[i].facultyList
            let optionsFaculty = [{value: null, text: 'ไม่เลือก'}]
            for(let j=0 ; j<facultyList.length ; j++) {
                optionsFaculty
                .push({
                    value: facultyList[j].faculty,
                    text: facultyList[j].faculty
                })
            }

            optionsFacultyOfCampus[`${campus}`] = optionsFaculty
        }

        this.setState({
            optionsProvince,
            optionsCampus,
            optionsFacultyOfCampus
        })

    }

    search = () => {
        let body = {}
        let data = this.state.data
        body = {
            ...body,
            ...data,
            roomTypeId: this.state.roomTypeId,
        }
        console.log(body)

        let apiList = [
            () => this.searchApi(body)
        ]

        this.callApiByParallel(apiList)

    }

    deleteRoom = (roomId) => {
        let body = {}
        let data = this.state.data
        body = {
            ...body,
            ...data,
            roomTypeId: this.state.roomTypeId,
        }
        let apiList = [
            () => this.daleteRoomApi(roomId),
            () => this.searchApi(body)
        ]

        // this.callApiByParallel(apiList)
        this.callApiBySequence(apiList)
    }

    searchApi = (body) => {
        return this.fetchData(
            'http://localhost:5000/room', 
            res => {
                this.setState({roomList: res.roomList})
                console.log(this.state.roomList)
            },
            'POST',
            {...body}
        )
    }

    daleteRoomApi = (roomId) => {
        return this.fetchData(
            `http://localhost:5000/room/delete/roomId/${roomId}`, 
            res => {
                console.log(res)
            },
            'POST'
        )
    }

/////////////this is function set/////////////

    callApiByParallel = (fetchFuncList) => {
        this.setState({
            responsePackageAll: fetchFuncList.length,
            isLoading: true
        })
        fetchFuncList.forEach( fetchFunc => {
            fetchFunc()
        })
        this.setState({
            isLoading: false
        })
    }

    callApiBySequence = (fetchFuncList, index=0) => {
        if(index === 0) {
            this.setState({
                responsePackageAll: fetchFuncList.length,
                isLoading: true
            })
        }
        if(!fetchFuncList[index]) {
            return this.setState({isLoading: false})
        }

        fetchFuncList[index]()
                .then( () => this.callApiBySequence(fetchFuncList, index+1))

    }

    fetchData = (url='/default', interactData=()=>{}, method='GET', data={}) => {
        return fetch(url, this.httpOption(method, data))
                .then(res => res.json())
                .then(res => {
                    interactData(res)
                    return res
                })
                .then( res => {
                    // let array = this.state.response
                    // array.push(res)
                    // this.setState({response: array})
                    this.setState({responsePackageReceive: this.state.responsePackageReceive+1})
                    return res
                })
    }

    httpOption = (method, data) => {
        let option = {
            method, // *GET, POST, PUT, DELETE, etc.
            // mode: "no-cors", // no-cors, cors, *same-origin
            // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
                // "ku-room-token": localStorage.getItem('ku-room-token')
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            // redirect: "follow", // manual, *follow, error
            // referrer: "no-referrer", // no-referrer, *client
        }

        const token = localStorage.getItem('ku-room-token')
        if(token) {
            option.headers['ku-room-token'] = token
        }

        let isMethodWithBody = false
        const methodWithBodyList = ['POST', 'PUT']
        methodWithBodyList.forEach( methodWithBody => {
            let logic = method === methodWithBody
            isMethodWithBody = isMethodWithBody || logic
        })
        if(isMethodWithBody) {
            option.body = JSON.stringify(data) // body data type must match "Content-Type" header
        }
        
        return option
    }

    redirect = (path='/', isReplacePageHistory=false) => {
        if(!isReplacePageHistory) this.props.history.push(path)
        else this.props.history.replace(path)
    }

/////////////this is function set/////////////
    handleChangeCampus = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            campus: value,
            faculty: null
        } 
    })
    handleChangeFaculty = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            faculty: value
        } 
    })

    handleChangeUsername = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            username: value
        } 
    })
    handleChangeProvince = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            province: value
        } 
    })
    handleChangeLocation = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            location: value
        } 
    })
    handleChangeBuildingName = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            buildingName: value
        } 
    })
    handleChangeRoomName = (e, { value }) => this.setState({ 
        data: {
            ...this.state.data, 
            roomName: value
        } 
    })

  

/////////////render set zone/////////////

    buttonSet = (onClick=()=>{}, content='default') => {
        return (
            <div>
                <Button positive onClick={()=>onClick()}>
                    {content}
                </Button>
            </div>
        )
    }

    standardRender = () => {
        let {data} = this.state
        return (
            <div>
                {data[0].roomName}
                {this.buttonSet(
                    () => this.redirect(),
                    'redirect'
                )}
            </div>
        )
    }

    changeRoomTypeId = (roomTypeId) => {
        this.setState({
            roomTypeId: roomTypeId,
        })
    }

    showData = () => {
        console.log(this.state)
    }

    renderButtonRoomTypeZone = () => {
        return ( 
            <Button.Group widths='3'>
                {this.renderButtonRoomType(1)}
                {this.renderButtonRoomType(2)}
                {this.renderButtonRoomType(3)}
            </Button.Group>
        )
    }

    renderButtonRoomType = (roomTypeId) => {
        if(this.state.roomTypeId === roomTypeId) {
            return (
                <Button color='blue' onClick={()=>this.changeRoomTypeId(roomTypeId)}>
                    {this.getRoomTypeName(roomTypeId)}
                </Button>
            )
        }
        else {
            return (
                <Button basic color='blue' onClick={()=>this.changeRoomTypeId(roomTypeId)}>
                    {this.getRoomTypeName(roomTypeId)}
                </Button>
            )
        }
    }

    getRoomTypeName = (roomTypeId) => {
        switch(roomTypeId) {
            case 1:
                return 'ห้องเรียน'
                break;
            case 2:
                return 'ห้องประชุม'
                break;
            case 3:
                return 'ห้องพัก'
                break;
            default:
                console.log('roomTypeId error')
                return null
        }
    }



    renderRoomListCard = () => {
        let roomListHtml = []

        let {roomList} = this.state
        roomList.forEach( room => {
            roomListHtml.push(this.renderRoomCard(room))
        })

        return (
            <Table celled>
                <Table.Header>
                <Table.Row>
                    <Table.HeaderCell width={2}>เจ้าของห้อง</Table.HeaderCell>
                    <Table.HeaderCell width={3}>อาคาร</Table.HeaderCell>
                    <Table.HeaderCell width={2}>ห้อง</Table.HeaderCell>
                    <Table.HeaderCell width={2}>จังหวัด</Table.HeaderCell>
                    <Table.HeaderCell width={5}>สถานที่</Table.HeaderCell>
                    <Table.HeaderCell width={1}>แก้ไข</Table.HeaderCell>
                    <Table.HeaderCell width={1}>ลบ</Table.HeaderCell>
                </Table.Row>
                </Table.Header>

                <Table.Body>

                    {roomListHtml}

                </Table.Body>
            </Table>
            
        )
    }

    renderRoomCard = (room) => {
        let {username, buildingName, roomName, province, location, roomId} = room
        return (
            <Table.Row>
                <Table.Cell>{username}</Table.Cell>
                <Table.Cell>{buildingName}</Table.Cell>
                <Table.Cell>{roomName}</Table.Cell>
                <Table.Cell>{province}</Table.Cell>
                <Table.Cell>{location}</Table.Cell>
                <Table.Cell style={{textAlign: 'center'}}><Icon className='pointer' name='edit' onClick={()=>this.redirect(`/admin/room/roomId/${roomId}`)}/></Table.Cell>
                <Table.Cell style={{textAlign: 'center'}}><Icon className='pointer' name='delete' onClick={()=>this.deleteRoom(roomId)}/></Table.Cell>
            </Table.Row>
        )

    }

/////////////this is render zone/////////////

    render() {
        let {isLoading} = this.state
        let {responsePackageReceive, responsePackageAll} = this.state

        if(isLoading || responsePackageReceive < responsePackageAll) return <Spinner/>
        /////////////
        let {roomTypeId, optionsProvince, optionsCampus, optionsFacultyOfCampus} = this.state
        let {username, location, province, buildingName, roomName, campus, faculty} = this.state.data

        let optionsFaculty = optionsFacultyOfCampus[`${campus}`]

        return (
            <div>
                <div style={{margin:'0vw 14vw'}}>
                    
                    {this.renderButtonRoomTypeZone()}

                    <Segment >          
                        <Form>
                            <Form.Group widths='equal'>
                            <Form.Field>
                                <label>เจ้าของห้อง</label>
                                <Input 
                                    fluid placeholder='username' 
                                    value={username}
                                    onChange={this.handleChangeUsername}
                                />
                            </Form.Field>
                            <Form.Field>
                                <label>อาคาร</label>
                                <Input 
                                    fluid placeholder='อาคาร' 
                                    value={buildingName}
                                    onChange={this.handleChangeBuildingName}
                                />
                            </Form.Field>
                            <Form.Field>
                                <label>ห้อง</label>
                                <Input 
                                    fluid placeholder='ห้อง' 
                                    value={roomName}
                                    onChange={this.handleChangeRoomName}
                                />
                            </Form.Field>
                            <Form.Field
                                control={Select}
                                searchInput={{ id: 'form-select-control-gender' }}
                                label={{ children: 'จังหวัด', htmlFor: 'form-select-control-gender' }}
                                placeholder='จังหวัด'
                                search

                                options={optionsProvince} 
                                value={province}
                                onChange={this.handleChangeProvince}
                            />
                            </Form.Group>
                            
                            <Form.Group widths='equal'>
                            
                            <Form.Field
                                control={Select}
                                searchInput={{ id: 'form-select-control-campus' }}
                                label={{ children: 'วิทยาเขต', htmlFor: 'form-select-control-campus' }}
                                placeholder='วิทยาเขต'
                                search

                                options={optionsCampus} 
                                value={campus}
                                onChange={this.handleChangeCampus}
                            />
                            <Form.Field
                                control={Select}
                                searchInput={{ id: 'form-select-control-faculty' }}
                                label={{ children: 'คณะ หน่วยงาน', htmlFor: 'form-select-control-faculty' }}
                                placeholder='คณะ หน่วยงาน'
                                search

                                options={optionsFaculty} 
                                value={faculty}
                                onChange={this.handleChangeFaculty}
                            />
                            <Form.Field>
                                <label>สถานที่</label>
                                <Input 
                                    fluid placeholder='สถานที่' 
                                    value={location}
                                    onChange={this.handleChangeLocation}
                                />
                            </Form.Field>
                            </Form.Group>

                            <Divider clearing />

                            <Button
                                style={{width:'100%'}}
                                color='blue'
                                attached='bottom'
                                content='Search'
                                onClick={this.search}
                            />

                        </Form>
                    </Segment>

                    {this.renderRoomListCard()}

                    {/* <Button
                        style={{width:'100%'}}
                        color='blue'
                        attached='bottom'
                        content='Test'
                        onClick={this.showData}
                    /> */}

                </div>
            </div>
        )
    }
}

export default withRouter(AdminRoomSearch)