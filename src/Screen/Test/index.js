// src/Component/Login/index.js
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import Spinner from '../../Component/Loader'

import { Button, Form, Segment, Image } from 'semantic-ui-react'

import './style.css';

class LoginTest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,

            response: [],
            responsePackageReceive: 0,
            responsePackageAll: 0,
            
            data: {
                username: '',
                password: ''
            },
            /////////////
            defaultUploadImage: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRO16IDz68_ChB0bL0KVMaqHOtB_ts835Io5cAWd40ZKS2QRL_w'
            
        }
    }

    testAuth = () => {
        return this.fetchData(
            'http://localhost:5000/post', 
            res => {console.log(res)},
            'POST',
        )
    }

    seeTokenData = () => {
        console.log(localStorage.getItem('ku-room-token'))
    }

    login = () => {
        let {data} = this.state
        console.log(data)
        return this.fetchData(
                    'http://localhost:5000/login', 
                    res => {
                        console.log(res)
                        if(res.token) {
                            localStorage.setItem('ku-room-token', res.token)
                        }
                    },
                    'POST',
                    {...data}
                )
    }

    logout = () => {
        localStorage.removeItem('ku-room-token')
    }

/////////////this is function set/////////////

    callApiByParallel = (fetchFuncList) => {
        this.setState({
            responsePackageAll: fetchFuncList.length,
            isLoading: true
        })
        fetchFuncList.forEach( fetchFunc => {
            fetchFunc()
        })
        this.setState({
            isLoading: false
        })
    }

    callApiBySequence = (fetchFuncList, index=0) => {
        if(index === 0) {
            this.setState({
                responsePackageAll: fetchFuncList.length,
                isLoading: true
            })
        }
        if(!fetchFuncList[index]) {
            return this.setState({isLoading: false})
        }

        fetchFuncList[index]()
                .then( () => this.callApiBySequence(fetchFuncList, index+1))

    }

    fetchData = (url='/default', interactData=()=>{}, method='GET', data={}) => {
        return fetch(url, this.httpOption(method, data))
                .then(res => res.json())
                .then(res => {
                    interactData(res)
                    return res
                })
                .then( res => {
                    // let array = this.state.response
                    // array.push(res)
                    // this.setState({response: array})
                    this.setState({responsePackageReceive: this.state.responsePackageReceive+1})
                    return res
                })
    }

    httpOption = (method, data) => {
        let option = {
            method, // *GET, POST, PUT, DELETE, etc.
            // mode: "no-cors", // no-cors, cors, *same-origin
            // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
                // "ku-room-token": localStorage.getItem('ku-room-token')
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            // redirect: "follow", // manual, *follow, error
            // referrer: "no-referrer", // no-referrer, *client
        }

        const token = localStorage.getItem('ku-room-token')
        if(token) {
            option.headers['ku-room-token'] = token
        }

        let isMethodWithBody = false
        const methodWithBodyList = ['POST', 'PUT']
        methodWithBodyList.forEach( methodWithBody => {
            let logic = method === methodWithBody
            isMethodWithBody = isMethodWithBody || logic
        })
        if(isMethodWithBody) {
            option.body = JSON.stringify(data) // body data type must match "Content-Type" header
        }
        
        return option
    }

    redirect = (path='/', isReplacePageHistory=false) => {
        if(!isReplacePageHistory) this.props.history.push(path)
        else this.props.history.replace(path)
    }

/////////////this is function set/////////////

    buttonSet = (onClick=()=>{}, content='default') => {
        return (
            <div>
                <Button positive onClick={()=>onClick()}>
                    {content}
                </Button>
            </div>
        )
    }

    standardRender = () => {
        let {data} = this.state
        return (
            <div>
                {data[0].roomName}
                {this.buttonSet(
                    () => this.redirect(),
                    'redirect'
                )}
            </div>
        )
    }

    handleChangeUsername = (event) => {
        let {data} = this.state
        this.setState({
            data: {
                ...data,
                username: event.target.value
            }
        })
    }
    
    handleChangePassword = (event) => {
        let {data} = this.state
        this.setState({
            data: {
                ...data,
                password: event.target.value
            }
        })
    }

    handleFileSubmit = (e) => {
        let files = e.target.files
        let reader = new FileReader()
        reader.readAsDataURL(files[0])
        reader.onload = (e)=>{
            this.setState({
                defaultUploadImage: e.target.result
            })
            console.log(e.target.result)
        }
    }

    render() {
        let {isLoading} = this.state
        let {responsePackageReceive, responsePackageAll} = this.state

        if(isLoading || responsePackageReceive < responsePackageAll) return <Spinner/>
        /////////////
        let {username, password} = this.state.data
        let {defaultUploadImage} = this.state

        return (
            <div>
                <Segment placeholder>
                    <Form>
                        <Form.Input icon='user' iconPosition='left' label='Username' placeholder='Username' 
                        value={username} onChange={this.handleChangeUsername}/>
                        <Form.Input icon='lock' iconPosition='left' label='Password' type='password' 
                        value={password} onChange={this.handleChangePassword}/>

                        <Button content='Login' primary  onClick={() => this.login()}/>
                        <Button content='token' onClick={() => this.seeTokenData()}/>
                        <Button content='auth' onClick={() => this.testAuth()}/>
                        <Button content='logout' onClick={() => this.logout()}/>
                    </Form>
                </Segment>
                <div>
                    <input type='file' name='file' onChange={this.handleFileSubmit} />
                    <Image src={defaultUploadImage} style={{height:'100px', width:'100px'}}/>
                </div>
            </div>
        )
    }
}

export default withRouter(LoginTest)